<h1>Pherrous</h1>

<h5>Discord bot engine made in Java using the JDA API.</h5>

<h3>Features:</h3>

- Reflective command loading
- User friendly backend
- "Bot" Administrator permissions system.

<h3>Overview</h3>
Pherrous was originally intended to replace serveral bots that all did different things with one that did everything.
Since then, it's grown into a larger project.

<h3>Commands (Stock):</h3>

<h5>Admin - Administrative commands.</h5>

- JoinRole:		Add role(s) to a user as soon as they join.
- Logging:		Basic action logging.
- PermCheck:	Role permission checker, used for detecting things such as people talking in announcements, admin channels, etc.
- Prune:		Clear out messages in a channel.
- ReactionRole:	Users can receive a role by adding a reaction to a message.
- SelfAssign:	Users can add predetermined roles to themselves via commands.

<h5>Fun - Fun/novel commands.</h5>

- BDE:			Big dick energy of a user.
- Bewareof:		Makes a fake "Beware of x" message. Inspired by those false text macros that users tend to fall for.
- Joke:			Get a joke.
- Magic8Ball:	Shake a magic 8 ball and get your question answered.
- Mimic:		Mimic a user using webhooks.
- Minesweeper:	Get a minesweeper board.
- MP3:			Get a mp3 file.
- OwOify:		Make a message cancerous.
- T:			Tags. Save a message with a key and then recall it again later with the key.

<h5>Gambling - Money, games, and a persistent economy.</h5>

- Money:		Money and currency managment. Currently not used by any other commands.

<h5>Imagemagick - Image editing</h5>

- Dontsign:		Get a sign that says "Don't xyz $350 fine".
- Paint:		Use imagemagick's "paint" modifier on an image.

<h5>Tools - Miscellanious tools and utilities.</h5>

- Avatar:		Get a user's avatar.A
- Botctl:		Manage the bot. Limited to operators and above.
- Help:			Returns a list of commands.
- Invite:		Generate an invite link for the bot.
- Man:			Similar to the unix man command, returns the syntax for a given command.
- Ping:			Get the bot's rest/api ping
- Status:		Server status.
- Whois:		Returns information on a user.

<h3>Commands (Commissioned):</h3>
<h5>Beanbot - A bot that posts beans</h5>

- Bean:			Get a arandom bean picture

<h5>Lizzy - DizzyDwarf's totally hot E-Girlfriend</h5>

- Jellybean:	Bullies the twang addict.
- Lizzy:		Invokes or controls responses.
- MapleLeaf:	Bullies the canadian.
- Pingu:		Spam ping a user.
- Twang:		Also bullies the twang addict.

<h5>Nonsense - When you need to shitpost at peak efficiency</h5>

- Nonsense:		Manages all facets of the module. Uses Markov chains to learn the speech patterns of a server, and then uses those to generate nonsense messages. (WARNING: THIS IS VERY RAM/IO/DISK INTENSIVE!)

<h5>ThanosYeezy - Shoe spam</h5>

- Yeezy:		Literally just spams a picture of Thanos photoshopped onto the heel of a shoe.
