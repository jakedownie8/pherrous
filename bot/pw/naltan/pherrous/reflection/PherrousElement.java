package pw.naltan.pherrous.reflection;

import java.util.ArrayList;

import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.interfaces.Timer;

public class PherrousElement {
	
	private static ArrayList<Class<?>> classes = new ArrayList<>();
	
	private Class<?> cls;
	private Class<?>[] interfaces;
	
	public PherrousElement(Class<?> cmd) {
		
		classes.add(cmd);
		this.cls = cmd;
		this.interfaces = cls.getInterfaces();
	}
	
	public Class<?> getAsClass() {
		
		return cls;
	}
	
	public String getName() {
		
		return cls.getName().substring(cls.getName().lastIndexOf(".") + 1, cls.getName().length());
	}
	
	public String getTrigger() {
		
		if (!isCommand() || !isManual()) {
			throw new IllegalArgumentException("This element is not a command or manual, therefore it doesn't have a name.");
		} else {
			try {
				Object obj = cls.newInstance();
				if (isCommand()) {
					GuildCommand gc = (GuildCommand) obj;
					return gc.name();
				} else if (isManual()) {
					Manual man = (Manual) obj;
					return man.name();
				}
			} catch (IllegalAccessException ex) {
				Console.error("The class " + cls.getSimpleName() + " cannot be accessed");
				Console.debug(ex);
			} catch (InstantiationException ex) {
				Console.error("The class " + cls.getSimpleName() + " cannot be instantiated.");
				Console.debug(ex);
			}
		}
		return null;
	}
	
	public boolean isCommand() {
		
		return checker(GuildCommand.class);
	}
	
	public GuildCommand asCommand() {
		
		return (GuildCommand) toObject(GuildCommand.class);
	}
	
	public boolean isTimer() {
		
		return checker(Timer.class);
	}
	
	public Timer asTimer() {
		
		return (Timer) toObject(Timer.class);
	}
	
	public boolean isIndexable() {
		
		return checker(Indexable.class);
	}
	
	public Indexable asIndexable() {
		
		return (Indexable) toObject(Indexable.class);
	}
	
	public boolean isManual() {
		
		return checker(Manual.class);
	}
	
	public Manual asManual() {
		
		return (Manual) toObject(Manual.class);
	}
	
	public boolean isPassthrough() {
		
		return checker(Passthrough.class);
	}
	
	public Passthrough asPassthrough() {
		
		return (Passthrough) toObject(Passthrough.class);
	}
	
	public boolean isAssets() {
		
		return checker(Assets.class);
	}
	
	public Assets asAssets() {
		
		return (Assets) toObject(Assets.class);
	}
	
	private Object toObject(Class<?> cast) {
		
		if (!checker(cast)) {
			throw new IllegalArgumentException(cls.getSimpleName() + " doesn't implement " + cast.getSimpleName());
			
		} else {
			try {
				return cls.newInstance();
			} catch (IllegalAccessException ex) {
				Console.error("The class " + cls.getSimpleName() + " cannot be accessed.");
				Console.debug(ex);
			} catch (InstantiationException ex) {
				Console.error("The class " + cls.getSimpleName() + " cannot be instantiated.");
				Console.debug(ex);
			}
		}
		return null;
	}
	
	private boolean checker(Class<?> cls) {
		
		for (Class<?> current : interfaces) {
			if (current.equals(cls)) {
				return true;
			}
		}
		return false;
	}
}
