package pw.naltan.pherrous.reflection;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PherrousJar {
	
	//private File f;
	private ArrayList<Class<?>> classes;
	private ArrayList<PherrousElement> elements;
	
	public PherrousJar(File f) {
		
		//this.f = f;
		classes = new ArrayList<>();
		elements = new ArrayList<>();
		
		try {
			//Init the jarfile and get all the entries
			JarFile jarFile = new JarFile(f.getPath());
			Enumeration<JarEntry> e = jarFile.entries();
			
			//Get the URLs and init the class loader
			URL[] urls = {
				new URL("jar:file:" + f.getPath() + "!/")
			};
			URLClassLoader cl = URLClassLoader.newInstance(urls);
			
			//For each, load the class if it is a .class
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				
				//if its a directory or not a jar, just move on
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				
				Class<?> cls = cl.loadClass(className);
				classes.add(cls);
				elements.add(new PherrousElement(cls));
				
			}
			jarFile.close();
			
		} catch (ClassNotFoundException | IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public ArrayList<PherrousElement> getElements() {
		
		return elements;
	}
}
