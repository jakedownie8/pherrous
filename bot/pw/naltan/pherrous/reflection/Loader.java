package pw.naltan.pherrous.reflection;

import java.io.File;

public class Loader {
	
	public static void loadJARS() {
		
		loadDir(new File("commands"));
	}
	
	public static void loadDir(File dir) {
		
		for (File current : dir.listFiles()) {
			
			if (current.isDirectory()) {
				loadDir(dir);
				
			} else if (current.isFile() && current.getName().toLowerCase().endsWith(".jar")) {
				Database.add(new PherrousJar(current));
			} else {
				System.out.println("Invalid file: " + current.getName());
			}
		}
	}
}
