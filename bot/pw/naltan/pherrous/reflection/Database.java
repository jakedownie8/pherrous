package pw.naltan.pherrous.reflection;

import java.util.ArrayList;

import pw.naltan.pherrous.commands.core.About;
import pw.naltan.pherrous.commands.core.Botctl;
import pw.naltan.pherrous.commands.core.Debug;
import pw.naltan.pherrous.commands.core.Help;
import pw.naltan.pherrous.commands.core.Invite;
import pw.naltan.pherrous.commands.core.Man;
import pw.naltan.pherrous.commands.core.Ping;
import pw.naltan.pherrous.commands.core.Status;
import pw.naltan.pherrous.commands.core.Whois;
import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Timer;
import pw.naltan.pherrous.objects.AssetKey;

public class Database {
	
	public static ArrayList<Assets> assets;
	public static ArrayList<GuildCommand> commands;
	public static ArrayList<Manual> manuals;
	public static ArrayList<Indexable> indexables;
	public static ArrayList<Timer> timers;
	public static ArrayList<Class<?>> passthroughs;
	
	private static void init() {
		
		assets = new ArrayList<>();
		commands = new ArrayList<>();
		manuals = new ArrayList<>();
		indexables = new ArrayList<>();
		passthroughs = new ArrayList<>();
		timers = new ArrayList<>();
	}
	
	public static void rehash() {
		
		errors = 0;
		ct = 0;
		
		init();
		Loader.loadJARS();
		loadDefaults();
		
		//load the asset directories
		for (Assets current : assets) {
			try {
				current.key(new AssetKey(current.assetDir()));
			} catch (Exception ex) {
				System.out.println("[ERROR]: Asset key with directory " + current.assetDir() + " threw an exception");
				Console.debug(ex);
			}
		}
		
		System.out.println("Finished (re)hashing " + ct + " commands with " + errors + " errors.");
		
	}
	
	public static void loadDefaults() {
		
		addDefault(new About());
		addDefault(new Botctl());
		addDefault(new Help());
		addDefault(new Invite());
		addDefault(new Man());
		addDefault(new Ping());
		addDefault(new Status());
		addDefault(new Whois());
		
		commands.add((GuildCommand) new Debug());
	}
	
	private static void addDefault(Object obj) {
		
		commands.add((GuildCommand) obj);
		manuals.add((Manual) obj);
		indexables.add((Indexable) obj);
	}
	
	private static int errors, ct;
	
	public static void add(PherrousJar obj) {
		
		try {
			for (PherrousElement current : obj.getElements()) {
				if (current.isAssets()) {
					assets.add(current.asAssets());
					Console.silent("+[AST] " + current.getName());
				}
				if (current.isCommand()) {
					commands.add(current.asCommand());
					Console.silent("+[CMD] " + current.getName());
				}
				if (current.isManual()) {
					manuals.add(current.asManual());
					Console.silent("+[MAN] " + current.getName());
				}
				if (current.isIndexable()) {
					indexables.add(current.asIndexable());
					Console.silent("+[HLP] " + current.getName());
				}
				if (current.isPassthrough()) {
					passthroughs.add(current.getAsClass());
					Console.silent("+[JDA] " + current.getName());
				}
				if (current.isTimer()) {
					timers.add(current.asTimer());
					Console.silent("+[CLK] " + current.getName());
				}
			}
			ct++;
		} catch (Exception ex) {
			Console.debug(ex);
			errors++;
		}
	}
	
}
