package pw.naltan.pherrous.util;

public class StrUtil {
	public static String capitalizeFirst(String in) {
		
		String out = "";
		String[] all = in.split(" ");
		for (String current : all) {
			String[] each = current.split("");
			out += each[0].toUpperCase();
			for (int i = 1; i < each.length; i++) {
				out += each[i].toLowerCase();
			}
		}
		return out;
	}
}
