package pw.naltan.pherrous.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

import pw.naltan.pherrous.core.output.Console;

public class JSONUtil {
	
	/**
	 * Parses a file into a JSONObject
	 * @param in - The file to parse
	 * @return A JSONObject with the file's contents.
	 * @throws FileNotFoundException The file specified wasn't found
	 * @throws JSONException The file isn't a JSON.
	 */
	public static JSONObject parse(File in) throws FileNotFoundException, JSONException {
		
		String out = "";
		Scanner s = new Scanner(in);
		while (s.hasNext()) {
			out += s.next();
		}
		s.close();
		
		return new JSONObject(out);
	}
	
	/**
	 * Writes a JSON file to the specified directory.
	 * @param obj - the JSONObject to write
	 * @param path - the path to write to
	 */
	public static void write(JSONObject obj, String path) {
		
		if (!path.toLowerCase().endsWith(".json")) {
			path += ".json";
		}
		
		try {
			String out = obj.toString(4);
			FileWriter fw = new FileWriter(new File(path));
			fw.write(out);
			fw.close();
		} catch (IOException ex) {
			Console.error("Attempt to write a JSON file at " + path + "failed.");
			Console.debug(ex);
		}
	}
}
