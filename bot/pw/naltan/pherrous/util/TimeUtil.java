package pw.naltan.pherrous.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.JSONObject;

public class TimeUtil {
	
	public static boolean isPast(File f, String key, long current) throws FileNotFoundException {
		
		try {
			JSONObject obj = JSONUtil.parse(f);
			long time = obj.getLong(key);
			
			//if the trigger time is less than the current time
			return time <= current;
			
		} catch (IOException ex) {
			return false;
		}
	}
	
	public static long parseTime(String in) {
		
		String[] arr = in.toLowerCase().split(" ");
		long out = 0;
		
		//m h d w
		for (String current : arr) {
			
			long val = isParseable(current);
			
			if (current.length() <= 1) {
				continue;
			}
			
			switch (current.split(" ")[current.length() - 1]) {
				case "m" :
					out += val;
					
				case "h" :
					out += val * 60;
					
				case "d" :
					out += val * 1440;
					
				case "w" :
					out += val * 10080;
			}
			
		}
		return out;
	}
	
	private static long isParseable(String in) {
		
		try {
			return Long.parseLong(in.substring(0, in.length() - 1));
		} catch (Exception ex) {
			return 0L;
		}
	}
}
