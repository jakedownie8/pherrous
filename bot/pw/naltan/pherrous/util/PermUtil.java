package pw.naltan.pherrous.util;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import pw.naltan.pherrous.settings.Settings;

public class PermUtil {
	
	/**
	 * Returns whether the member is an operator or not.
	 * @param member The member to be checked.
	 * @return A boolean dictating whether they're an operator or not.
	 */
	public static boolean isOperator(Member member) {
		
		return (PermUtil.isOperator(member.getUser()) || isOwner(member));
	}
	
	/**
	 * Returns whether the user is an operator or not.
	 * @param user The user to be checked.
	 * @return A boolean dictating whether they're an operator or not.
	 */
	public static boolean isOperator(User user) {
		
		return (Settings.getOperators().contains(user.getId()) || isOwner(user));
	}
	
	/**
	 * Returns whether the member is the owner or not.
	 * @param member The member to be checked.
	 * @return A boolean dictating whether they're the owner or not.
	 */
	public static boolean isOwner(Member member) {
		
		return isOwner(member.getUser());
	}
	
	/**
	 * Returns whether the user is the owner or not.
	 * @param user The user to be checked.
	 * @return A boolean dictating whether they're the owner or not.
	 */
	public static boolean isOwner(User user) {
		
		return (Settings.getOwner().equals(user.getId()));
	}
}
