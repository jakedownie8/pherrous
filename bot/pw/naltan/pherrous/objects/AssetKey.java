package pw.naltan.pherrous.objects;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONObject;
import pw.naltan.pherrous.util.JSONUtil;

public class AssetKey {
	
	private String name;
	
	private String dir;
	
	public AssetKey(String name) {
		
		this.name = name;
		dir = "data/" + name + "/";
		File f = new File(dir);
		if (!f.isDirectory()) {
			f.mkdirs();
		}
	}
	
	public String getName() {
		
		return name;
	}
	
	public String getDir() {
		
		return dir;
	}
	
	public File getByName(String name) {
		
		File f = new File(dir + name);
		if (!f.exists()) {
			return null;
		}
		return f;
	}
	
	/**
	 * Gets a file. Returns null if it doesn't exist
	 * @param file the file's name
	 * @return the file, or null if it doesn't exist.
	 */
	public File getFile(String file) {
		
		try {
			File req = new File(dir + file);
			if (!req.exists()) {
				throw new NullPointerException();
			}
			return req;
		} catch (NullPointerException | IllegalArgumentException ex) {
			return null;
		}
	}
	
	/**
	 * Reads from the JSON file provided in ./data/[name]/
	 * @param key Your asset key.
	 * @param path - The path of the json file.
	 * @return A possibly empty JSONObject, or the one requested. If the file doesn't exist, it creates the file.
	 */
	public JSONObject readJSON(String path) {
		
		if (!path.toLowerCase().endsWith(".json")) {
			path += ".json";
		}
		File f = new File(path);
		try {
			return JSONUtil.parse(f);
		} catch (FileNotFoundException ex) {
			touchJSON(path, new JSONObject());
			return new JSONObject();
		}
	}
	
	public void writeJSON(String path, JSONObject obj) {
		
		if (!path.toLowerCase().endsWith(".json")) {
			path += ".json";
		}
		JSONUtil.write(obj, path);
	}
	
	/**
	 * Creates a JSON file with the name provided
	 * @param key Your asset key
	 * @param filename the name of the file
	 * @param def the JSONObject to write
	 */
	public void touchJSON(String filename, JSONObject def) {
		
		String path = dir;
		
		try {
			if (!filename.toLowerCase().endsWith(".json")) {
				filename += ".json";
			}
			File f = new File(path + filename);
			File dir = new File(path);
			
			if (f.exists()) {
				return;
			} else if (f.isDirectory()) {
				f.delete();
			}
			f.mkdirs();
			f.delete();
			f.createNewFile();
			
			dir.mkdirs();
			if (def != null) {
				FileWriter fw = new FileWriter(f);
				/*
				if (def == null) {
					fw.write("{}");
					fw.close();
					return;
				}
				*/
				fw.write(def.toString(4));
				fw.close();
			}
		} catch (IOException ex) {
			System.out.println("An IOException occured while initializing the file writer");
			ex.printStackTrace();
		}
	}
}
