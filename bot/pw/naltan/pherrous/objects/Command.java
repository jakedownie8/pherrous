package pw.naltan.pherrous.objects;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import pw.naltan.pherrous.settings.Settings;

public class Command extends GuildMessageReceivedEvent {
	
	public Command(GuildMessageReceivedEvent e) {
		
		super(e.getJDA(), e.getResponseNumber(), e.getMessage());
		
	}
	
	//OUTPUT
	
	public void sendTempMessage(String message, int time) {
		
		super.getChannel().sendMessage(message).queue(cons -> {
			try {
				Thread.sleep(time);
			} catch (Exception ex) {}
			cons.delete().queue();
		});
	}
	
	public void sendMessage(String message) {
		
		super.getChannel().sendMessage(message).queue();
	}
	
	public MessageAction queueMessage(String message) {
		
		return super.getChannel().sendMessage(message);
	}
	
	public void sendMessage(EmbedBuilder eb) {
		
		super.getChannel().sendMessage(eb.setColor(Settings.getColor()).build()).queue();
	}
	
	public MessageAction queueMessage(EmbedBuilder eb) {
		
		return super.getChannel().sendMessage(eb.setColor(Settings.getColor()).build());
	}
	
	public void sendMessage(File f) {
		
		super.getChannel().sendFile(f).queue();
	}
	
	public MessageAction queueMessage(File f) {
		
		return super.getChannel().sendFile(f);
	}
	
	public void sendPM(String message, User user) {
		
		user.openPrivateChannel().queue(consumer -> {
			consumer.sendMessage(message).queue();
		});
	}
	
	public void addReaction(Emote e) {
		
		super.getMessage().addReaction(e).queue();
	}
	
	public void addReaction(String e) {
		
		super.getMessage().addReaction(e).queue();
	}
	
	public void addReaction(Emote e, Message m) {
		
		m.addReaction(e).queue();
	}
	
	public void addReaction(String e, Message m) {
		
		m.addReaction(e).queue();
	}
	
	public void sendManual(ManualBuilder man) {
		
		sendMessage(man.build());
	}
	//INPUT
	
	public boolean checkCmd(String cmd) {
		
		return checkIndex(0, Settings.getPrefix() + cmd);
	}
	
	public boolean checkIndex(int index, String check) {
		
		try {
			return getMessageArray()[index].equalsIgnoreCase(check);
		} catch (Exception ex) {
			return false;
		}
	}
	
	public String[] getMessageArray() {
		
		return super.getMessage().getContentDisplay().split(" ");
	}
	
	public boolean checkMan(String man) {
		
		return checkCmd("man") && checkIndex(1, man);
	}
	
	public String getRestOfMessage(int index) {
		
		String[] msg = super.getMessage().getContentRaw().split(" ");
		String out = "";
		for (int i = index; i != msg.length; i++) {
			out += msg[i] + " ";
		}
		return out.trim();
	}
	
	/**
	 * Returns the text at the specified index, or an empty string if nothing was there.
	 * @param index - the index to check
	 * @return the String at the index
	 */
	public String getIndex(int index) {
		
		try {
			return super.getMessage().getContentDisplay().split(" ")[index].toLowerCase();
		} catch (Exception ex) {
			return "";
		}
	}
	
	public String getIndex(int index, boolean throwException) {
		
		String indx = getIndex(index);
		if (indx.equals("")) {
			if (throwException) {
				throw new ArrayIndexOutOfBoundsException();
			} else {
				return indx;
			}
		}
		return indx;
	}
	
	public File downloadPostedImage(int index, AssetKey key) {
		
		if (!super.getMessage().getAttachments().isEmpty()) {
			String name = super.getMessage().getAttachments().get(index).getFileName().toLowerCase();
			if (!validImgExt(name)) {
				return null;
			}
			String extension = name.substring(name.lastIndexOf("."));
			File f = new File(key.getDir() + "download" + extension);
			if (f.exists() || f.isDirectory()) {
				f.delete();
			}
			super.getMessage().getAttachments().get(0).downloadToFile(f);
			return f;
		}
		return null;
	}
	
	public File downloadOnlineImage(String in, AssetKey key) {
		
		File out = null;
		
		for (String url : in.split(" ")) {
			do {
				try {
					URLConnection conn = new URL(url).openConnection();
					conn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
					
					BufferedInputStream inputStream = new BufferedInputStream(conn.getInputStream());
					FileOutputStream fileOS = new FileOutputStream(key.getDir() + "download" + getExtension(url));
					byte data[] = new byte[1024];
					int byteContent;
					
					while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
						fileOS.write(data, 0, byteContent);
					}
					fileOS.close();
					out = new File(key.getDir() + "download" + getExtension(url));
					
				} catch (IOException ex) {
					return null;
				}
				
			} while (out == null);
		}
		return out;
	}
	
	private static boolean validImgExt(String in) {
		
		String ext = getExtension(in);
		String[] valid = new String[]{
			"png",
			"jpg",
			"jpeg",
			"gif",
			"svg",
			"tiff",
			"bmp",
			"pdf",
		};
		for (String current : valid) {
			if (ext.endsWith("." + current)) {
				return true;
			}
		}
		return false;
	}
	
	public static String getExtension(String in) {
		
		int point = in.lastIndexOf(".");
		String ext = in.toLowerCase().substring(point);
		
		while (ext.lastIndexOf(".") < ext.lastIndexOf("?")) {
			ext = ext.substring(0, ext.lastIndexOf("?"));
		}
		
		//ext = ext.substring(0, ext.lastIndexOf("?"));
		
		return ext;
	}
	
	public Emote getEmote(int index) {
		
		try {
			return super.getMessage().getEmotes().get(index);
		} catch (Exception ex) {
			return null;
		}
	}
	
	public File downloadEmote(Emote e, AssetKey key) {
		
		return downloadOnlineImage(e.getImageUrl(), key);
	}
	
	public File downloadAvatar(User u, AssetKey key) {
		
		return downloadOnlineImage(u.getEffectiveAvatarUrl(), key);
	}
	
	public File getLastEffectiveImage(Message m, AssetKey key, boolean loop) {
		
		try {
			File out;
			boolean getout = true;
			while (getout) {
				getout = false;
				
				out = downloadPostedImage(0, key);
				if (out != null)
					return out;
				
				out = downloadOnlineImage(m.getContentDisplay(), key);
				if (out != null)
					return out;
				
				try {
					User u = m.getMentionedUsers().get(0);
					out = downloadAvatar(u, key);
					if (out != null)
						return out;
				} catch (Exception ex) {}
				
				out = downloadEmote(getEmote(0), key);
				if (out != null)
					return out;
				/*
				out = getEmoji(e, key);
				if (out != null)
					throw new Exception();
				*/
			}
			
			if (!loop) {
				return null;
			} else {
				MessageHistory hist = MessageHistory.getHistoryBefore(super.getChannel(), super.getMessage().getId()).limit(50).complete();
				List<Message> list = hist.getRetrievedHistory(); //hist list
				//int count = 0;
				for (Message current : list) {
					
					//System.out.println(count++ + " - " + current.getId());
					File temp = getLastEffectiveImage(current, key, false);
					if (temp != null) {
						//System.out.println("Located. Downloading...");
						return temp;
					}
				}
			}
			System.out.println("not found");
			return null;
		} catch (Exception ex) {
			return null;
		}
	}
	
}
