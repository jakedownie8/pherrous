package pw.naltan.pherrous.objects;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import pw.naltan.pherrous.settings.Settings;
import pw.naltan.pherrous.util.StrUtil;

public class ManualBuilder {
	
	private String ManualVersion = "2.1";
	
	private String command;
	
	private String description;
	
	private String author;
	
	private String version;
	
	private Color color;
	
	private List<String> args = new ArrayList<String>();
	
	private List<String> desc = new ArrayList<String>();
	
	/**
	 * Manual Builder, for the {@link core.interfaces.external.interfaces.Manual manual} interface.
	 * @param command The name of the command.
	 */
	public ManualBuilder(String command) {
		
		this.command = command;
	}
	
	/**
	 * Get the description of the command.
	 * @return A possibly null string with the description.
	 */
	public String getDescription() {
		
		return description;
	}
	
	/**
	 * Sets the description of the command.
	 * @param The description The description to set.
	 * @return this.
	 */
	public ManualBuilder setDescription(String description) {
		
		this.description = description;
		return this;
	}
	
	/**
	 * Gets the author.
	 * @return A possibly null string containing the author's name.
	 */
	public String getAuthor() {
		
		return author;
	}
	
	/**
	 * Sets the author's name.
	 * @param author The author's name.
	 * @return this.
	 */
	public ManualBuilder setAuthor(String author) {
		
		this.author = author;
		return this;
	}
	
	/**
	 * Gets the current version of the command.
	 * @return A possibly null string containing the command's current version.
	 */
	public String getVersion() {
		
		return version;
	}
	
	/**
	 * Set the version of the command.
	 * @param version The version to set.
	 * @return this.
	 */
	public ManualBuilder setVersion(String version) {
		
		this.version = version;
		return this;
	}
	
	/**
	 * Get the current ManualBuilder as a message embed.
	 * @return A MessageEmbed manual.
	 */
	public MessageEmbed getMessageEmbed() {
		
		return build().build();
	}
	
	/**
	 * <br>Add an argument to a command. Will appear as the following:
	 * <br>~command argument
	 * <br>     does x y and z
	 * 
	 * 
	 * @param newarg the full command and argument to add.
	 * @param desc description for the argument.
	 * @return this.
	 */
	public ManualBuilder addArg(String newarg, String desc) {
		
		args.add(newarg);
		this.desc.add(desc);
		return this;
	}
	
	/**
	 * Builds the manual into an EmbedBuilder.
	 * @return The manual as an EmbedBuilder.
	 */
	public EmbedBuilder build() {
		
		try {
			EmbedBuilder eb = new EmbedBuilder();
			eb.setTitle("Manual for " + StrUtil.capitalizeFirst(command));
			eb.setDescription("**Description:** *" + description + "*" + "\n**Author:** *" + author + "*" + "\n**Version:** *" + version + "*");
			
			if (!args.isEmpty()) {
				int noargs = args.size();
				
				String argstr = "```";
				
				for (int i = 0; i != noargs; i++) {
					argstr += "" + Settings.getPrefix() + command + " " + args.get(i) + "\n     " + desc.get(i) + "\n\n";
				}
				argstr += "```";
				eb.addField("Arguments", argstr, true);
			}
			
			/*
			 * ~command <arg>
			 *       "this arg does this, and accepts this this and this.
			 * ~command <arg>
			 * 
			 */
			
			eb.setFooter("Pherrous manual v. " + ManualVersion, null);
			if (color == null) { //if the color was never specified
				color = Settings.getColor();
			}
			if (color == null) { //if the default color is null somehow
				color = new Color(100, 100, 100);
			}
			eb.setColor(color);
			return eb;
		} catch (NullPointerException ex) {
			EmbedBuilder eb = new EmbedBuilder();
			eb.setTitle("Error").setDescription("The creator of this command is missing a field");
			
			return eb;
		}
	}
}
