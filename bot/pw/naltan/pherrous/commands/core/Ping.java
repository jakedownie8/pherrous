package pw.naltan.pherrous.commands.core;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Ping implements GuildCommand, Manual, Indexable {
	
	@Override
	public String name() {
		
		return "ping";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get the bot's ping").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public void cmd(Command e) {
		
		e.getJDA().getRestPing().queue(ping -> {
			String msg = "`Gateway: " + e.getJDA().getGatewayPing() + "ms`\n";
			msg += "`Rest:    " + ping + "ms`";
			
			EmbedBuilder eb = new EmbedBuilder();
			eb.setTitle("Pong!").setDescription(msg).setThumbnail("http://static.naltan.pw/cdn/ping.gif");;
			
			e.sendMessage(eb);
		});
		
	}
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
}
