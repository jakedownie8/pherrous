package pw.naltan.pherrous.commands.core;

import java.text.Collator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.settings.Settings;
import pw.naltan.pherrous.util.StrUtil;

public class Help implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setAuthor("Naltan").setDescription("Get a list of my commands").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "help";
	}
	
	@Override
	public void cmd(Command e) {
		
		//Get all categories and remove duplicates
		Set<String> cats = new HashSet<String>();
		for (Indexable current : Database.indexables) {
			cats.add(current.category().toLowerCase());
		}
		
		//Sort the categories alphabetically
		Collection<String> categories = new TreeSet<String>(Collator.getInstance());
		for (String current : cats) {
			categories.add(current);
		}
		
		EmbedBuilder eb = new EmbedBuilder();
		//For each category...
		for (String current : categories) {
			String list = "";
			//...iterate through all the commands marked indexable...
			for (Indexable currentcmd : Database.indexables) {
				//...if the current command's category is the current category being iterated through...
				if (currentcmd.category().toLowerCase().equals(current)) {
					//...add it to the list.
					list += "-" + StrUtil.capitalizeFirst(currentcmd.name()) + "\n";
				}
			}
			//once done, add a field for the category that isn't inline.
			eb.addField(StrUtil.capitalizeFirst(current), list, true);
		}
		eb.setTitle("List of commands:").setFooter("Do \"" + Settings.getPrefix() + "man <command>\" to learn the usage of a command", null);
		
		e.sendMessage(eb);
	}
	
}
