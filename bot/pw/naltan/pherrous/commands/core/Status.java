package pw.naltan.pherrous.commands.core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import pw.naltan.pherrous.core.head.Bot;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.PermUtil;

public class Status extends Bot implements GuildCommand, Manual, Indexable, Assets {
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Status.key = key;
	}
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get some statistics about Pherrous")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("servers", "Get a list of servers Pherrous is on. Operator+ Only.")
			.addArg("users", "Get a list of every unique user that Pherrous is serving. Operator+ Only.");
	}
	
	@Override
	public String name() {
		
		return "status";
	}
	
	@Override
	public void cmd(Command e) {
		
		switch (e.getIndex(1)) {
			case "servers" :
				servers(e);
				break;
			
			case "users" :
				users(e);
				break;
			
			default :
				def(e);
		}
		
	}
	
	private void users(Command e) {
		
		try {
			checkOperator(e);
			
			e.sendMessage("Compiling... this may take a bit...");
			File f = new File(key.getDir() + "users.txt");
			if (f.exists()) {
				f.delete();
			}
			f.createNewFile();
			
			FileWriter fw = new FileWriter(f, true);
			
			for (Member current : getMems(e)) {
				String app = current.getUser().getId() + " " + current.getUser().getName() + "#" + current.getUser().getDiscriminator() + "\r\n";
				fw.append(app);
			}
			fw.close();
			e.getAuthor().openPrivateChannel().queue(privChan -> {
				privChan.sendFile(f).queue();
				f.delete();
			});
			
		} catch (IOException ex) {
			
		} catch (IllegalArgumentException ex) {
			e.sendMessage("You do not have permission to use this command");
		}
	}
	
	private void servers(Command e) {
		
		try {
			checkOperator(e);
			
			e.sendMessage("Compiling... this may take a bit...");
			File f = new File(key.getDir() + "servers.txt");
			if (f.exists()) {
				f.delete();
			}
			f.createNewFile();
			
			FileWriter fw = new FileWriter(f, true);
			
			for (Guild current : e.getJDA().getGuilds()) {
				String app = current.getId() + " " + current.getName() + "\r\n";
				fw.append(app + "\n");
			}
			fw.close();
			e.getAuthor().openPrivateChannel().queue(privChan -> {
				privChan.sendFile(f).queue();
				f.delete();
			});
			
		} catch (IOException ex) {
			
		} catch (IllegalArgumentException ex) {
			e.sendMessage("You do not have permission to use this command");
		}
	}
	
	private void checkOperator(Command e) {
		
		if (!PermUtil.isOperator(e.getAuthor())) {
			throw new IllegalArgumentException();
		}
	}
	
	private void def(Command e) {
		
		Set<Member> mems = new HashSet<Member>();
		
		mems = getMems(e);
		int guilds = e.getJDA().getGuilds().size();
		
		e.sendMessage("Serving " + mems.size() + " unique members on " + guilds + " guilds.");
	}
	
	private Set<Member> getMems(Command e) {
		
		Set<Member> mems = new HashSet<Member>();
		for (Guild current : e.getJDA().getGuilds()) {
			mems.addAll(current.getMembers());
			
		}
		return mems;
	}
	
	@Override
	public String assetDir() {
		
		return "status";
	}
	
}
