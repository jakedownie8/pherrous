package pw.naltan.pherrous.commands.core;

import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.util.StrUtil;

public class Man extends Database implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get the manual for a command").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "man";
	}
	
	@Override
	public void cmd(Command e) {
		
		for (Manual current : Database.manuals) {
			if (e.checkIndex(1, current.name())) {
				ManualBuilder man = new ManualBuilder(current.name());
				e.sendManual(current.man(man));
				return;
			}
		}
		e.sendMessage("No manual found for \"" + StrUtil.capitalizeFirst(e.getIndex(1)) + "\"");
		
	}
	
}
