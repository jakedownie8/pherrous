package pw.naltan.pherrous.commands.core;

import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Invite implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man
			
			.setDescription("Get my invite link")
			.setAuthor("Nathan")
			.setVersion("1.0")
		
		;
		
	}
	
	@Override
	public String name() {
		
		return "invite";
	}
	
	@Override
	public void cmd(Command e) {
		
		e.sendMessage("Invite me using this link:\nhttps://discordapp.com/api/oauth2/authorize?client_id=" + e.getGuild().getSelfMember().getUser().getId()
			+ "&permissions=0&scope=bot");
	}
	
}
