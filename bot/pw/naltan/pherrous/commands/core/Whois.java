package pw.naltan.pherrous.commands.core;

import java.time.format.DateTimeFormatter;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.PermUtil;

public class Whois implements GuildCommand, Manual, Indexable {
	
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		man.setDescription("Get information on a user").setAuthor("Naltan").setVersion("1.0");
		return man;
	}
	
	@Override
	public String name() {
		
		return "whois";
	}
	
	@Override
	public void cmd(Command e) {
		
		Member member = null;
		
		try {
			member = e.getMessage().getMentionedMembers().get(0);
		} catch (IndexOutOfBoundsException ex) {
			member = e.getMember();
		}
		
		String nick = member.getNickname();
		if (nick == null) {
			nick = member.getEffectiveName();
		}
		
		String general = "**Nickname:** " + nick;
		general += "\n**Global Name:** " + member.getUser().getName();
		general += "\n**User ID: **" + member.getUser().getId();
		general += "\n**Discriminator: ** " + member.getUser().getDiscriminator();
		general += "\n**Join Date:** " + member.getTimeJoined().format(dtf);
		general += "\n**Account Birthday:** " + member.getUser().getTimeCreated().format(dtf);
		
		String vals = "**Is a bot:** " + member.getUser().isBot();
		vals += "\n**Is a Webhook:** " + member.getUser().isFake();
		vals += "\n**Is an Admin:** " + member.hasPermission(Permission.ADMINISTRATOR);
		vals += "\n**Is server owner:** " + member.isOwner();
		vals += "\n**Is an operator:** " + PermUtil.isOperator(member);
		vals += "\n**Is bot owner:** " + PermUtil.isOwner(member);
		
		vals = vals.replace("true", "Yes").replace("false", "No");
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("Info on " + nick);
		eb.setThumbnail(member.getUser().getEffectiveAvatarUrl());
		eb.addField("User Info", general, true);
		eb.addField("Permissions", vals, true);
		
		e.sendMessage(eb);
	}
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
}
