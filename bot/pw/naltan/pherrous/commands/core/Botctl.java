package pw.naltan.pherrous.commands.core;

import net.dv8tion.jda.api.entities.Activity;
import pw.naltan.pherrous.core.head.Bot;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.util.PermUtil;

public class Botctl extends Bot implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "pherrous";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Control some aspects of the bot. Operator+ only.")
			.setAuthor("Naltan")
			.setVersion("1.0")
			.addArg("shutdown", "Shut down the bot. Add --full to shut down the program as well.")
			.addArg("presence <watching/playing/listening/streaming> <status>", "Set the bot's presence")
			.addArg("rehash", "Rehash the available commands")
		
		;
	}
	
	@Override
	public String name() {
		
		return "botctl";
	}
	
	@Override
	public void cmd(Command e) {
		
		if (!PermUtil.isOperator(e.getMember())) {
			e.sendMessage("You do not have permission to use this command");
			return;
		}
		
		String botctl = "";
		try {
			botctl = e.getMessage().getContentRaw().split(" ")[1].toLowerCase();
		} catch (ArrayIndexOutOfBoundsException ex) {}
		switch (botctl) {
			
			case "shutdown" :
				shutdown(e);
				return;
			
			case "presence" :
				presence(e);
				return;
			
			case "memory" :
				memory(e);
				return;
			
			case "rehash" :
				rehash(e);
				return;
			
			default :
				e.sendMessage("That is not a valid argument.");
				return;
		}
	}
	
	private void rehash(Command e) {
		
		if (!PermUtil.isOperator(e.getAuthor())) {
			e.sendMessage("Only operators can do this.");
			return;
		}
		Database.rehash();
		e.sendMessage("Rehashed available commands.");
	}
	
	private void memory(Command e) {
		
		double total = Runtime.getRuntime().totalMemory() / (double) (1024 * 1024);
		double max = Runtime.getRuntime().maxMemory() / (double) (1024 * 1024);
		double free = Runtime.getRuntime().freeMemory() / (double) (1024 * 1024);
		
		String out = String.format("total: %.2fMB\nmax: %.2fMB\nfree: %.2fMB", total, max, free);
		
		e.sendMessage("```" + out + "```");
	}
	
	private void shutdown(Command e) {
		
		if (e.getMessage().getContentRaw().toLowerCase().contains("--full")) {
			e.sendMessage("Fully exiting...");
			System.exit(1);
		}
		
		e.sendMessage("Shutting down...");
		Bot.stop();
	}
	
	private void presence(Command e) {
		
		String presence = ""; //Presence is the presence (listening/playing/watching)
		String value = ""; //Whatever they're setting the presence to say (watching **you**, playing **Arma 2**)
		try {
			presence = e.getMessage().getContentRaw().split(" ")[2].toLowerCase(); //presence is index 2
			value = e.getRestOfMessage(3); //value is index 3+
			
			if (value.equals("")) { //if it's empty, throw an exception
				throw new ArrayIndexOutOfBoundsException();
			}
		} catch (ArrayIndexOutOfBoundsException ex) { //catch exception, e man, return.
			ManualBuilder man = new ManualBuilder("Botctl");
			e.sendMessage(man(man).build());
			
		}
		String response = "";
		
		/*
		 * Just a bunch of switches for the possible outcomes.
		 * if the presence isn't valid, say its invalid and return.
		 */
		
		switch (presence) {
			case "listening" : //listening to <value>
				e.getJDA().getPresence().setActivity(Activity.listening(value));
				response = "Listening to";
				break;
			
			case "playing" : //playing <value>
				e.getJDA().getPresence().setActivity(Activity.playing(value));
				response = "Playing";
				break;
			
			case "watching" : //watching <value>
				e.getJDA().getPresence().setActivity(Activity.watching(value));
				response = "Watching";
				break;
			
			case "streaming" : //streaming <value> (Comes with the twitch presence)
				e.getJDA().getPresence().setActivity(Activity.streaming(value, null));
				response = "Streaming";
				break;
			
			default :
				e.sendMessage("That is not a valid presence");
				return;
		}
		
		e.sendMessage("Set presence to \"" + response + " " + value + "\"");
	}
	
}
