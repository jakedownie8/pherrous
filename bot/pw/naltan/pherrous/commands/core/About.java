package pw.naltan.pherrous.commands.core;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class About implements GuildCommand, Manual, Indexable {
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get information about me").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "about";
	}
	
	/**
	 * Prints out info on Pherrous.
	 */
	@Override
	public void cmd(Command e) {
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("About Me");
		String desc = "```" + "AUTHOR:      Nathan \n" + "SUPPORT:     nathan@naltan.pw \n" + "SOURCE:      https://gitlab.com/Naltan/pherrous"
		
		;
		
		eb.setDescription(desc);
		
		e.sendMessage(eb);
	}
	
	@Override
	public String category() {
		
		return "pherrous";
	}
}
