package pw.naltan.pherrous.settings;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Scanner;

public class Setup {
	
	private static Scanner s = new Scanner(System.in);
	
	public static void setupAll() {
		
		System.out.println("Pherrous full setup");
		setupToken();
		setupOwner();
		setupOperators();
		setupPrefix();
		setupColor();
		
		Settings.writeValues();
	}
	
	public static void setupToken() {
		
		String entry1 = "";
		String entry2 = "a";
		do {
			
			System.out.println("Enter the token to use (1/2)");
			entry1 = s.next();
			System.out.println("Enter the token again for verification (2/2)");
			entry2 = s.next();
			
			if (!entry1.equals(entry2)) {
				System.out.println("The two tokens do not match. Try again.");
			} else {
				Settings.Token = entry1;
				System.out.println("Token \"" + Settings.Token + "\" saved.\n");
			}
			
		} while (!entry1.equals(entry2));
		
	}
	
	public static void setupOwner() {
		
		System.out.println("Enter the User ID for my owner");
		Settings.Owner = s.next();
		System.out.println("Owner ID \"" + Settings.Owner + "\" saved.\n");
	}
	
	public static void setupOperators() {
		
		String key = "";
		
		ArrayList<String> ops = new ArrayList<>();
		
		System.out.println("Enter the User IDs for my operators.\nEnter \"done\" when finished.");
		do {
			printOps(ops);
			key = s.next();
			
			if (key.equalsIgnoreCase("done")) {
				//nada
			} else if (ops.contains(key)) {
				ops.remove(key);
				System.out.println("Removed " + key);
			} else {
				ops.add(key);
				System.out.println("Added " + key);
			}
			
		} while (!key.equalsIgnoreCase("done"));
		
		Settings.Operators = ops;
		System.out.println("Updated the operators list.\n");
		
	}
	
	private static void printOps(ArrayList<String> ops) {
		
		for (String current : ops) {
			System.out.println(current);
		}
	}
	
	public static void setupPrefix() {
		
		System.out.println("Enter the prefix I should use.");
		String e = "";
		do {
			
			System.out.println("Invalid prefixes: *, ~~, __, ||");
			e = s.next();
			
		} while (invalidPrefix(e));
		
		Settings.Prefix = e;
		System.out.println("Saved \"" + e + "\" as the prefix.");
		
	}
	
	public static void setupColor() {
		
		boolean loop = true;
		
		System.out.println("Enter the color to use");
		
		int r = 0;
		int g = 0;
		int b = 0;
		
		do {
			
			String[] in = s.nextLine().split(" ");
			try {
				loop = false;
				
				r = Integer.parseInt(in[0]);
				g = Integer.parseInt(in[1]);
				b = Integer.parseInt(in[2]);
				
				if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255) {
					throw new Exception();
					
				}
				
			} catch (Exception ex) {
				loop = true;
				System.out.println("0-255 R, 0-255 G, 0-255 B");
			}
			
		} while (loop);
		Settings.Color = new Color(r, g, b);
		System.out.println("Saved color as (" + r + ", " + g + ", " + b + ")");
	}
	
	private static boolean invalidPrefix(String in) {
		
		String[] invalid = {
			"~~",
			"__",
			"||",
		};
		
		for (String current : invalid) {
			if (in.equals(current) || in.startsWith("*")) {
				return true;
			}
		}
		return false;
	}
}
