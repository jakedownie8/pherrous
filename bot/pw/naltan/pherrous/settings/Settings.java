package pw.naltan.pherrous.settings;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import pw.naltan.pherrous.util.JSONUtil;

public class Settings {
	
	public static void dirCheck() {
		
		File[] f = new File[4];
		f[0] = new File("logs");
		f[1] = new File("commands");
		f[2] = new File("data");
		f[3] = new File("pherrous");
		
		boolean[] b = new boolean[4];
		
		for (int i = 0; i < 4; i++) {
			if (!f[i].exists()) {
				f[i].mkdirs();
				b[i] = true;
			} else if (!f[i].isDirectory()) {
				f[i].delete();
				f[i].mkdirs();
				b[i] = true;
			} else {
				b[i] = false;
			}
		}
		
		if (b[0] || b[1] || b[2] || b[3]) {
			Setup.setupAll();
		}
	}
	
	protected static String Token;
	protected static String Prefix;
	protected static String Owner;
	protected static ArrayList<String> Operators = new ArrayList<>();
	protected static Color Color;
	protected static JSONObject obj;
	
	public static void init() {
		
		obj = new JSONObject();
		
		try {
			obj = JSONUtil.parse(new File("pherrous/settings.json"));
			
			Token = obj.getString("token");
			Prefix = obj.getString("prefix");
			Owner = obj.getString("owner");
			
			JSONArray color = obj.getJSONArray("color");
			Color = new Color(color.getInt(0), color.getInt(1), color.getInt(2));
			
			JSONArray ops = obj.getJSONArray("operators");
			Iterator<Object> i = ops.iterator();
			while (i.hasNext()) {
				Operators.add((String) i.next());
			}
			
		} catch (Exception ex) {
			Setup.setupAll();
		}
	}
	
	public static void writeValues() {
		if(obj == null) {
			obj = new JSONObject();
		}
		obj.put("token", Token);
		obj.put("prefix", Prefix);
		obj.put("owner", Owner);
		JSONArray ops = new JSONArray();
		for (String current : Operators) {
			ops.put(current);
		}
		obj.put("operators", ops);
		JSONArray color = new JSONArray();
		color.put(Color.getRed());
		color.put(Color.getGreen());
		color.put(Color.getBlue());
		obj.put("color", color);
		
		JSONUtil.write(obj, "pherrous/settings.json");
		
	}
	
	public static String getPrefix() {
		
		return Prefix;
	}
	
	public static String getOwner() {
		
		return Owner;
	}
	
	public static String getToken() {
		
		return Token;
	}
	
	public static Color getColor() {
		
		return Color;
	}
	
	public static ArrayList<String> getOperators() {
		
		return Operators;
	}
}
