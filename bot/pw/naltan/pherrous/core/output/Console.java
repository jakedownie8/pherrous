package pw.naltan.pherrous.core.output;

import java.io.OutputStream;
import java.io.PrintStream;

import pw.naltan.pherrous.core.head.Args;

public class Console {
	
	public static void debug(Exception ex) {
		
		if (Args.debug()) {
			sysErrOn();
			ex.printStackTrace();
			sysErrOff();
		}
	}
	
	public static void debug(String msg) {
		
		if (Args.debug()) {
			System.out.println("[DEBUG]: " + msg);
		}
	}
	
	public static void error(String msg) {
		
		System.out.print("[ERROR]: " + msg);
	}
	
	public static void silent(String msg) {
		
		if (!Args.silent()) {
			System.out.println(msg);
		}
	}
	
	public static final PrintStream ERR = System.err;
	
	public static void sysErrOff() {
		
		if (Args.debug()) {
			return;
		}
		System.setErr(new PrintStream(new OutputStream() {
			
			@Override
			public void write(int b) {
				
				//DO NOTHING
			}
		}));
	}
	
	public static void sysErrOn() {
		
		if (Args.debug()) {
			return;
		}
		System.setErr(ERR);
	}
}
