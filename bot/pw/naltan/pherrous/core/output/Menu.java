package pw.naltan.pherrous.core.output;

import java.util.ArrayList;
import java.util.Scanner;

import pw.naltan.pherrous.core.head.Args;
import pw.naltan.pherrous.core.head.Bot;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.settings.Settings;
import pw.naltan.pherrous.settings.Setup;
import pw.naltan.pherrous.util.StrUtil;

public class Menu {
	
	private String menu;
	private ArrayList<String> items;
	private static final String LINE = "----------------";
	
	public Menu(String header) {
		
		menu = header;
		items = new ArrayList<>();
	}
	
	public void clear() {
		
		items = new ArrayList<>();
	}
	
	public Menu addItem(String item) {
		
		items.add(item);
		return this;
	}
	
	public Menu addItem(String item, int index) {
		
		items.add(index, item);
		return this;
	}
	
	public String build() {
		
		String out = menu;
		for (int i = 0; i < items.size(); i++) {
			out += "\n-" + (i + 1) + ": " + items.get(i);
		}
		return out;
	}
	
	static Scanner s = new Scanner(System.in);
	
	public static void mainMenu(boolean justLaunch) {
		
		int selection = -1;
		
		Menu m = new Menu("\nMain Menu\n" + LINE);
		m.addItem("Launch");
		m.addItem("Edit Settings");
		m.addItem("Credits");
		m.addItem("Support");
		m.addItem("Get Flags");
		m.addItem("Exit");
		
		do {
			
			System.out.println(m.build());
			if (!justLaunch) {
				try {
					selection = Integer.parseInt(s.next());
				} catch (NumberFormatException ex) {
					selection = -1;
				}
			} else {
				justLaunch = false;
				selection = 1;
			}
			
			System.out.println(); //just for niceness
			switch (selection) {
				case 1 :
					System.out.println("Please wait...");
					Bot.start();
					botMenu();
					break;
				
				case 2 :
					settingsMenu();
					break;
				
				case 3 :
					System.out.println("Made by Nathan_#0001\nhttp://naltan.pw");
					break;
				
				case 4 :
					System.out.println("Join this server to contact me\nfNzE5ek");
					break;
				
				case 5 :
					flags();
					break;
				
				case 6 :
					System.out.println("Exiting...");
					System.exit(0);
					
				default :
					System.out.println("Invalid option...");
			}
			
		} while (true);
		
	}
	
	private static void flags() {
		
		System.out.println("Debug: " + Args.debug());
		System.out.println("Quick: " + Args.quick());
		System.out.println("Silent: " + Args.silent());
	}
	
	public static void botMenu() {
		
		int selection = -1;
		
		Menu m = new Menu("\nBot Menu\n" + LINE);
		m.addItem("Shutdown");
		m.addItem("Restart");
		m.addItem("Rehash");
		m.addItem("Edit Settings");
		m.addItem("List Commands");
		
		do {
			
			System.out.println(m.build() + "\n");
			
			try {
				selection = Integer.parseInt(s.next());
			} catch (NumberFormatException ex) {
				selection = -1;
			}
			
			switch (selection) {
				
				case 1 :
					Bot.stop();
					return;
				
				case 2 :
					Bot.restart();
					break;
				
				case 3 :
					Database.rehash();
					break;
				
				case 4 :
					settingsMenu();
					break;
				
				case 5 :
					listCmd();
					break;
				
				default :
					System.out.println("Invalid option...");
					
			}
			
		} while (true);
	}
	
	private static void listCmd() {
		
		for (GuildCommand current : Database.commands) {
			System.out.println(StrUtil.capitalizeFirst(current.name()));
		}
	}
	
	public static void settingsMenu() {
		
		int selection = -1;
		
		Menu m = new Menu("Settings\n" + LINE);
		m.addItem("Go Back");
		m.addItem("Change Token");
		m.addItem("Change Owner");
		m.addItem("Change Color");
		m.addItem("Change Prefix");
		m.addItem("Add/Remove Operators");
		
		do {
			
			System.out.println(m.build() + "\n");
			
			try {
				selection = Integer.parseInt(s.next());
			} catch (NumberFormatException ex) {
				selection = -1;
			}
			
			switch (selection) {
				case 1 :
					return;
				
				case 2 :
					Setup.setupToken();
					break;
				
				case 3 :
					Setup.setupOwner();
					break;
				
				case 4 :
					Setup.setupColor();
					break;
				
				case 5 :
					Setup.setupPrefix();
					break;
				
				case 6 :
					Setup.setupOperators();
					break;
				
				default :
					System.out.println("Invalid option...");
			}
			Settings.writeValues();
			
		} while (true);
	}
	
}
