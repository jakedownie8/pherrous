package pw.naltan.pherrous.core.head;

public class Args {
	
	private static boolean silent = false;
	private static boolean quick = false;
	private static boolean debug = false;
	
	public Args(String[] args) {
		
		for (String current : args) {
			switch (current.toLowerCase()) {
				case "-s" :
					System.out.println("Silent mode on");
					silent = true;
					break;
				
				case "--silent" :
					System.out.println("Silent mode on");
					silent = true;
					break;
				
				case "-q" :
					System.out.println("Quicklaunch mode on");
					quick = true;
					break;
				
				case "--quick" :
					System.out.println("Quicklaunch mode on");
					quick = true;
					break;
				
				case "-d" :
					System.out.println("Debug mode on");
					debug = true;
					break;
				
				case "--debug" :
					System.out.println("Debug mode on");
					debug = true;
					break;
				
			}
		}
	}
	
	public static boolean debug() {
		
		return debug;
	}
	
	public static boolean silent() {
		
		return silent;
	}
	
	public static boolean quick() {
		
		return quick;
	}
	
}
