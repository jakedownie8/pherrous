package pw.naltan.pherrous.core.head;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.loader.commands.CommandLoader;
import pw.naltan.pherrous.loader.time.Time;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.settings.Settings;

public class Bot extends ListenerAdapter {
	
	private static boolean isLaunched;
	private static JDA api;
	private static byte launches = 0;
	
	public static JDA getJDA() {
		
		return api;
	}
	
	public static void start() {
		
		try {
			launches++;
			isLaunched = true;
			Database.rehash();
			
			api = new JDABuilder(AccountType.BOT).setToken(Settings.getToken()).build();
			
			api.addEventListener(new CommandLoader());
			
			//add all the custom listeners
			for (Class<?> current : Database.passthroughs) {
				try {
					api.addEventListener(current.newInstance());
				} catch (Exception ex) {
					Console.error("The class " + current.getName() + " could not be loaded.");
					Console.debug(ex);
				}
			}
			
			api.awaitReady();
			
			//Start the timer thread
			Thread time = new Time(launches);
			time.start();
			Console.debug("Time thread initialized");
			
		} catch (LoginException ex) {
			launches--;
			System.out.println("The token that was entered isn't working. Double-check it.");
			isLaunched = false;
		} catch (InterruptedException ex) {
			launches--;
			isLaunched = false;
			Console.debug("An interrupted exception occured while waiting for the bot to connect...");
			Console.debug(ex);
		}
	}
	
	public static void stop() {
		
		isLaunched = false;
		api.shutdown();
	}
	
	public static void restart() {
		
		stop();
		start();
	}
	
	public static boolean isLaunched() {
		
		return isLaunched;
	}
	
	public static byte getLaunchNumber() {
		
		return launches;
	}
}
