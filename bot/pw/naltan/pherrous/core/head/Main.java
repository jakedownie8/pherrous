package pw.naltan.pherrous.core.head;

import java.io.File;

import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.core.output.Menu;
import pw.naltan.pherrous.settings.Settings;

public class Main {
	
	private static final File LOCK = new File(".pherrous.lock");
	
	public static void main(String[] args) {
		
		lockThread();
		addShutdownHook();
		
		try {
			new Args(args);
			
			Console.sysErrOff(); //turn off exceptions. if -d isn't specified.
			
			Settings.dirCheck(); //check that the directories required exist. if not, create and startup
			Settings.init(); //init the settings. if this fails, setup and init.
			
			Menu.mainMenu(Args.quick()); //main menu, if quicklaunch, then just launch
			
		} catch (Exception ex) {
			
			Console.sysErrOn();
			Console.debug(ex);
			ex.printStackTrace();
			Console.sysErrOff();
		}
	}
	
	private static void addShutdownHook() {
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			
			public void run() {
				
				System.out.println("Thanks for using me!");
				
				if (LOCK.exists()) {
					LOCK.delete();
				}
				
			}
		});
	}
	
	private static void lockThread() {
		
		if (LOCK.exists()) {
			System.out.println("ERROR: Pherrous is already running.");
			System.out.println("Please make sure all instances of Pherrous are shut down before running in order to prevent errors");
			System.out.println("HINT: If you're sure I'm not running or want to run two instances, delete the file '.pherrous.lock'.");
			System.exit(0);
		} else {
			try {
				LOCK.createNewFile();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
