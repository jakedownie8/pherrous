package pw.naltan.pherrous.loader.time;

import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.Timer;

public class TimeThreader implements Runnable {
	
	private Timer timer;
	private long time;
	
	public TimeThreader(Timer timer, long time) {
		
		this.timer = timer;
		this.time = time;
	}
	
	@Override
	public void run() {
		
		try {
			timer.onTick(time);
		} catch (Exception ex) {
			Console.error("Timer " + timer.getClass().getName() + " failed to run");
			Console.debug(ex);
		}
	}
	
}
