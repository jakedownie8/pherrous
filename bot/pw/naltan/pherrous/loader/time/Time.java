package pw.naltan.pherrous.loader.time;

import java.time.Instant;

import pw.naltan.pherrous.core.head.Bot;
import pw.naltan.pherrous.interfaces.Timer;
import pw.naltan.pherrous.reflection.Database;

public class Time extends Thread {
	
	private byte launch;
	
	public Time(byte launch) {
		
		this.launch = launch;
	}
	
	public void run() {
		
		do {
			long min = Instant.now().getEpochSecond() / 60L;
			
			for (Timer current : Database.timers) {
				new TimeThreader(current, min).run();
			}
			try {
				for (int i = 0; i < 120; i++) {
					Thread.sleep(500);
					if (!Bot.isLaunched()) {
						return;
					}
				}
			} catch (InterruptedException ex) {}
			
		} while (Bot.isLaunched() && Bot.getLaunchNumber() == launch);
		
	}
}
