package pw.naltan.pherrous.loader.commands;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.PermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.reflection.Database;
import pw.naltan.pherrous.settings.Settings;

public class CommandLoader extends ListenerAdapter {
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
		
		if (e.getAuthor().isBot() || e.getAuthor().isFake()) {
			return;
		}
		
		Command c = new Command(e);
		
		String command = c.getMessageArray()[0];
		try {
			for (GuildCommand current : Database.commands) {
				if (command.equalsIgnoreCase(Settings.getPrefix() + current.name())) {
					
					CommandThreader t = new CommandThreader(c, current);
					
					t.start();
				}
			}
		} catch (PermissionException ex) {
			if (!(ex instanceof HierarchyException)) {
				c.sendMessage("ERROR: I need the permission " + ex.getPermission().getName() + " in order to do that");
			} else {
				Console.debug(ex);
			}
		}
	}
	
	@Override
	public void onGuildMessageUpdate(GuildMessageUpdateEvent e) {
		
		GuildMessageReceivedEvent ev = new GuildMessageReceivedEvent(e.getJDA(), e.getResponseNumber(), e.getMessage());
		
		this.onGuildMessageReceived(ev);
	}
	
}
