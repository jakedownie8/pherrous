package pw.naltan.pherrous.loader.commands;

import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import pw.naltan.pherrous.core.head.Args;
import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.objects.Command;

public class CommandThreader extends Thread {
	
	public CommandThreader(Command c, GuildCommand cmd) {
		
		this.cmd = cmd;
		this.c = c;
	}
	
	private GuildCommand cmd;
	private Command c;
	
	@Override
	
	public void run() {
		
		long start = 0;
		long end = 0;
		if (Args.debug()) {
			start = System.currentTimeMillis();
		}
		try {
			cmd.cmd(c);
		} catch (Exception ex) {
			
			if (ex instanceof InsufficientPermissionException) {
				InsufficientPermissionException pe = (InsufficientPermissionException) ex;
				
				c.sendMessage("ERROR: I need the permission \"" + pe.getPermission().getName() + "\" to do that.");
				
			}
			
			Console.error("Command " + cmd.name() + " failed to run \n");
			Console.debug(ex);
		}
		
		if (Args.debug()) {
			end = System.currentTimeMillis();
			
			Console.debug(c.getIndex(0) + " ran in " + (end - start) + " ms");
		}
		
	}
	
}
