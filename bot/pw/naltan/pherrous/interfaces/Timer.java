package pw.naltan.pherrous.interfaces;

public interface Timer {
	
	void onTick(long time);
}
