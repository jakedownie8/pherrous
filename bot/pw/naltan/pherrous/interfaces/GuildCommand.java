package pw.naltan.pherrous.interfaces;

import pw.naltan.pherrous.objects.Command;

public interface GuildCommand {
	
	/**
	 * The name of your command, which will be executed if a message equals the prefix + this string.
	 * 
	 * @return Command trigger.
	 */
	String name();
	
	/**
	 * Fires when both a {@link net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent GuildMessageReceivedEvent} is fired, and the command matches the {@link core.interfaces.external.interfaces.GuildCommand.name Name} in your file.
	 * 
	 * @param e The {@link net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent GuildMessageReceivedEvent}.
	 */
	void cmd(Command e);
	
}
