package pw.naltan.pherrous.interfaces;

import pw.naltan.pherrous.objects.ManualBuilder;

public interface Manual {
	
	/**
	 * This is the trigger that returns the manual for your command. This should be the same trigger as the {@link core.interfaces.external.interfaces.GuildCommand GuildCommand} trigger.
	 * 
	 * @return The trigger for your command.
	 */
	String name();
	
	/**
	 * The manual for your command. This is kind of like the bash manual, which is just short and to the point documentation on a command. Fires when someone does ~man {@link core.interfaces.external.interfaces.Manual.name your command name} (assuming that the prefix is ~)
	 * @param e The {@link net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent GuildMessageReceivedEvent}.
	 * @return a filled out {@link pw.naltan.pherrous.objects.external.builders.ManualBuilder.ManualBuilder ManualBuilder}
	 */
	ManualBuilder man(ManualBuilder man);
}
