package pw.naltan.pherrous.interfaces;

public interface Indexable {
	
	/**
	 * Help indexer. While it's not required, I highly reccommend implementing this if you're creating a command. This basically just tells the help command to add this to its list of commands.
	 * 
	 * @return The name of your command, which will be presented in the help command.
	 */
	String name();
	
	/**
	 * Manual category. If no category is specified, then it will go in to the "miscellanious" category be default.
	 */
	String category();
}
