package pw.naltan.pherrous.interfaces;

import pw.naltan.pherrous.objects.AssetKey;

public interface Assets {
	
	/**
	 * Allows for an assets folder. On run, if the dir doesn't exist, it creates it.
	 * @return name of the directory, should be the same as the command.
	 */
	String assetDir();
	
	/**
	 * The asset key. Use this in the file loader with the file name.
	 * @param key your asset key.
	 */
	void key(AssetKey key);
}
