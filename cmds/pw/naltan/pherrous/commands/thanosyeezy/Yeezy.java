package pw.naltan.pherrous.commands.thanosyeezy;

import java.io.File;

import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Yeezy implements GuildCommand, Manual, Indexable, Assets {
	
	@Override
	public String category() {
		
		return "yeezy";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man
			
			.setDescription("THANOS YEEZY")
			.setAuthor("Nathan, Snoipah")
			.setVersion("1.0")
			.addArg("x", "Post x amount of yeezies. Min 1, Max 25");
	}
	
	@Override
	public String name() {
		
		return "yeezy";
	}
	
	@Override
	public void cmd(Command e) {
		
		int times = 1;
		try {
			times = Integer.parseInt(e.getMessageArray()[1]);
			
			if (times <= 0) {
				times = 1;
			}
			
			if (times > 25) {
				times = 25;
			}
			
		} catch (Exception ex) {}
		
		for (int i = 0; i < times; i++) {
			if (y == null) {
				e.sendMessage("THANOS YEEZY");
			} else {
				e.sendMessage(y);
			}
			
			try {
				Thread.sleep(250);
			} catch (Exception ex) {}
			
		}
		
	}
	
	@Override
	public String assetDir() {
		
		return "yeezy";
	}
	
	private static File y;
	
	@Override
	public void key(AssetKey key) {
		
		try {
			y = key.getFile("yeezy.jpg");
		} catch (Exception ex) {
			y = null;
		}
	}
}
