package pw.naltan.pherrous.commands.lizzy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Lizzy extends ListenerAdapter implements GuildCommand, Manual, Indexable, Assets, Passthrough {
	
	public void onGuildMessageReceived(Command e) {
		
		if (e.getAuthor().getId().equals("251216694503145472") && new Random().nextInt(100) < freq) {
			invoke(e);
		}
	}
	
	@Override
	public String category() {
		
		return "lizzy";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Bully Dizzy")
			.setAuthor("Nathan")
			.setVersion("3.0")
			.addArg("add <response>", "Add a response")
			.addArg("remove <response>", "remove a response")
			.addArg("list", "List responses")
			.addArg("invoke", "Invoke a random response");
	}
	
	@Override
	public String name() {
		
		return "lizzy";
	}
	
	@Override
	public void cmd(Command e) {
		
		switch (e.getIndex(1)) {
			case "add" :
				add(e);
				break;
			
			case "remove" :
				remove(e);
				break;
			
			case "freq" :
				freq(e);
				break;
			
			case "list" :
				list(e);
				break;
			
			case "invoke" :
				invoke(e);
				break;
			
			default :
				invoke(e);
		}
	}
	
	private void freq(Command e) {
		
		try {
			int freq = Integer.parseInt(e.getIndex(2));
			
			if (freq > 100) {
				freq = 100;
			} else if (freq <= 0) {
				freq = 1;
			}
			
			Lizzy.freq = freq;
			e.sendMessage("Set the frequency to " + freq + "%");
			
		} catch (NumberFormatException ex) {
			e.sendMessage("Not a Number");
		} catch (ArrayIndexOutOfBoundsException ex) {
			e.sendMessage("Enter a value");
		}
	}
	
	private void remove(Command e) {
		
		String[] rm = e.getMessage().getContentRaw().split(" ");
		String msg = "";
		for (int i = 2; i < rm.length; i++) {
			msg += rm[i] + " ";
		}
		msg = msg.trim();
		
		boolean success = responses.remove(msg);
		String out = "Failed to remove that message";
		if (success) {
			out = "Sucessfully removed that message.";
		}
		writeJSON();
		e.sendMessage(out);
	}
	
	private void list(Command e) {
		
		String msg = "My resposne chance is " + freq + "%.";
		if (responses.size() > 0) {
			msg += "\n```";
			for (String current : responses) {
				if (msg.length() + current.length() < 1995) {
					msg += "\n" + current;
				}
			}
		}
		e.sendMessage(msg + "```");
	}
	
	private void add(Command e) {
		
		String[] arr = e.getMessage().getContentRaw().split(" ");
		String out = "";
		for (int i = 2; i < arr.length; i++) {
			out += arr[i] + " ";
		}
		out = out.trim();
		responses.add(out);
		writeJSON();
		e.sendMessage("Added: ```" + out + "```");
	}
	
	private void invoke(Command e) {
		
		if (responses.size() == 0) {
			e.sendMessage("binch");
			return;
		}
		int rand = new Random().nextInt(responses.size());
		e.sendMessage(responses.get(rand));
	}
	
	@Override
	public String assetDir() {
		
		return "lizzy";
	}
	
	private static List<String> responses = new ArrayList<>();
	private static int freq = 25;
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Lizzy.key = key;
		readJSON();
		
	}
	
	private void readJSON() {
		
		try {
			JSONObject obj = key.readJSON("lizzy");
			
			JSONArray resp = obj.getJSONArray("responses");
			
			if (resp.isEmpty()) {
				writeJSON();
			} else {
				for (Object current : resp) {
					responses.add((String) current);
				}
				freq = obj.getInt("freq");
			}
		} catch (Exception ex) {
			writeJSON();
		}
	}
	
	private void writeJSON() {
		
		JSONArray arr = new JSONArray();
		for (String current : responses) {
			arr.put(current);
		}
		if (responses.isEmpty()) {
			responses.add("binch");
			arr.put("binch");
		}
		JSONObject obj = new JSONObject();
		obj.put("responses", arr);
		obj.put("freq", freq);
		key.writeJSON("lizzy", obj);
	}
}
