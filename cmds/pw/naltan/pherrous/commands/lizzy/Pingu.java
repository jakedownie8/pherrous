package pw.naltan.pherrous.commands.lizzy;

import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Pingu implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "lizzy";
	}
	
	@Override
	public String name() {
		
		return "pingu";
	}
	
	private String[] blacklist = {
		"191393241961201664",
		"276167849775464448",
	};
	
	@Override
	public void cmd(Command e) {
		
		int count = 0;
		try {
			count = Integer.parseInt(e.getMessage().getContentRaw().split(" ")[2]);
			if (count <= 0 || count > 25)
				throw new NumberFormatException();
		} catch (Exception ex) {
			e.sendManual(man(new ManualBuilder("Pingu")));
			return;
		}
		
		try {
			User target = e.getMessage().getMentionedUsers().get(0);
			
			for (String current : blacklist) {
				if (e.getAuthor().getId().equals(current) || target.getId().equals(current)) {
					e.sendMessage("This user is blacklisted from this command.");
					return;
				}
			}
			
			for (int i = 0; i != count; i++) {
				e.sendMessage(target.getAsMention());
				Thread.sleep(2500);
			}
			return;
			
		} catch (Exception ex) {
			try {
				Role target = e.getMessage().getMentionedRoles().get(0);
				
				for (int i = 0; i != count; i++) {
					e.sendMessage(target.getAsMention());
					Thread.sleep(2500);
				}
				return;
				
			} catch (Exception exc) {
				e.sendManual(man(new ManualBuilder("Pingu")));
				return;
			}
		}
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Spam ping a user. Limit of 25 pings.")
			.setAuthor("Nathan")
			.setVersion("2.0")
			.addArg("@user <number>", "Ping a user @user <number> times.")
			.addArg("@role <number>", "Ping a role @role <number> times.");
	}
}
