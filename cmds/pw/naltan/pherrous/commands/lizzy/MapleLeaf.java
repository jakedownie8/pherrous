package pw.naltan.pherrous.commands.lizzy;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.Command;

public class MapleLeaf extends ListenerAdapter implements Passthrough {
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
		
		if (e.getAuthor().getId().equals("88764104193368064")) {
			new Command(e).addReaction("🍁");
		}
	}
	
}
