package pw.naltan.pherrous.commands.lizzy;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.Command;

public class Jellybean extends ListenerAdapter implements Passthrough {
	
	public void onGuildMessageReceived(GuildMessageReceivedEvent e) {
		
		if (e.getAuthor().getId().equals("245694294084681729")) {
			new Command(e).addReaction("🐦");
		}
	}
}
