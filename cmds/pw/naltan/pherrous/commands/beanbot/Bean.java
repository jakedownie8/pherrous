package pw.naltan.pherrous.commands.beanbot;

import java.io.File;
import java.util.Random;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message.Attachment;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Bean implements GuildCommand, Manual, Indexable, Assets {
	
	@Override
	public String category() {
		
		return "beans";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get beaned").setAuthor("Nathan").setVersion("2.0").addArg("", "Get beaned").addArg("add", "Attach a file to this message to add that bean");
		
	}
	
	@Override
	public String name() {
		
		return "bean";
	}
	
	@Override
	public void cmd(Command e) {
		
		switch (e.getIndex(1)) {
			case "add" :
				add(e);
				break;
			
			default :
				def(e);
		}
		
	}
	
	private void add(Command e) {
		
		if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
			e.sendMessage("Only admins can add beans");
			return;
		}
		
		try {
			String name = e.getMessageId();
			Attachment dl = e.getMessage().getAttachments().get(0);
			String dlname = dl.getFileName();
			name += dlname.substring(dlname.lastIndexOf(".") + 1, dlname.length());
			File f = new File(key.getDir() + name);
			dl.downloadToFile().complete(f);
			
			String msg = "Failed to download that image. Try again.";
			if (f.exists()) {
				msg = "Successfully added " + name;
			}
			e.sendMessage(msg);
			
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("Attach a file to this message");
		}
	}
	
	private void def(Command e) {
		
		File bdir = new File(key.getDir());
		File[] beans = bdir.listFiles();
		File choice = beans[new Random().nextInt(beans.length)];
		e.sendMessage(choice);
	}
	
	@Override
	public String assetDir() {
		
		return "beans";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Bean.key = key;
	}
}
