package pw.naltan.pherrous.commands.nonsense;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import pw.naltan.pherrous.commands.lib.nonsense.MarkovChain;
import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Nonsense implements GuildCommand, Manual, Indexable, Assets {
	
	@Override
	public String assetDir() {
		
		return "nonsense";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Nonsense.key = key;
		
	}
	
	@Override
	public String category() {
		
		return "nonsense";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		man.setDescription("Generate a markov-chain nonsense message");
		man.setAuthor("Nathan");
		man.setVersion("1.0");
		
		return man;
	}
	
	@Override
	public String name() {
		
		return "nonsense";
	}
	
	private static HashMap<String, MarkovChain> chain = new HashMap<String, MarkovChain>();
	
	@Override
	public void cmd(Command e) {
		
		ArrayList<File> fs = new ArrayList<>();
		for (File current : new File(key.getDir()).listFiles()) {
			fs.add(current);
		}
		File f = new File("foobar");
		for (File current : fs) {
			if (current.getName().startsWith(e.getGuild().getId())) {
				f = current;
				break;
			}
		}
		
		String[] msg = e.getMessageArray();
		
		if (msg.length > 1 && msg[1].equalsIgnoreCase("cache")) {
			if (e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
				cache(e);
				e.sendMessage("Finished!");
				return;
			} else {
				e.sendMessage("Only administrators can do that.");
				return;
			}
		}
		if (f.exists()) {
			nonsenseExec(e, fs);
		} else {
			e.sendMessage("ERROR: This guild hasn't been cached. Tell an administrator.");
		}
		
	}
	
	private static void nonsenseExec(Command e, ArrayList<File> fs) {
		
		e.getChannel().sendTyping().queue();
		
		ArrayList<File> list = new ArrayList<>();
		for (File current : fs) {
			if (current.getName().startsWith(e.getGuild().getId())) {
				list.add(current);
			}
		}
		
		int lowbound = 3;
		int highbound = 10;
		int ruleset = 1;
		
		int chance = ((int) (Math.random() * (highbound - lowbound))) + lowbound;
		
		try {
			if (chain.get(e.getGuild().getId()) == null) {
				
				MarkovChain temp = new MarkovChain(list, ruleset, chance);
				temp.map();
				chain.put(e.getGuild().getId(), temp);
				
			}
			
			e.sendMessage(chain.get(e.getGuild().getId()).nextString().replace("@", "@​"));
			
		} catch (IOException ex) {
			e.sendMessage("```" + ex + "```");
		}
	}
	
	private static void cache(Command e) {
		
		e.getChannel().sendMessage("Caching " + e.getGuild().getName() + ", this may take a while...").queue();
		ArrayList<TextChannel> c = new ArrayList<TextChannel>();
		List<TextChannel> immute = e.getGuild().getTextChannels();
		
		for (TextChannel current : immute) {
			if (!current.isNSFW() && current.canTalk()) {
				c.add(current);
			}
		}
		
		for (TextChannel current : c) {
			num = 1;
			Console.debug("Caching " + current.getName() + "...");
			cacheChannel(current);
		}
	}
	
	private static void cacheChannel(TextChannel c) {
		
		int i = 0;
		String msg = "";
		for (Message current : c.getIterableHistory()) {
			String temp = current.getContentRaw()
				//Replacing CR, NL, and any other fuckery
				.replace("\n", " ")
				.replace("\r", " ")
				.replace("\t", " ")
				.replace("  ", " ");
			msg += temp + " ";
			
			i++;
			if (i == 50) {
				i = 0;
				save(msg, c);
				msg = "";
			}
		}
		save(msg, c);
	};
	
	private static int num = 1;
	
	private static void save(String str, TextChannel c) {
		
		//guild-channel-1234.txt
		String path = c.getGuild().getId() + "-" + c.getId() + "-" + num + ".txt";
		num++;
		File f = new File(key.getDir() + path);
		try {
			FileWriter fw = new FileWriter(f);
			fw.write(str);
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
