package pw.naltan.pherrous.commands.pherrous.tools;

import net.dv8tion.jda.api.entities.User;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Avatar implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Returns a user's avatar, or your own if no user is specified.")
			.setAuthor("Naltan")
			.setVersion("1.0")
			.addArg("<@user>", "Get the avatar of the mentioned user");
	}
	
	@Override
	public String name() {
		
		return "avatar";
	}
	
	@Override
	public void cmd(Command e) {
		
		User user = null;
		try {
			user = e.getMessage().getMentionedUsers().get(0);
		} catch (IndexOutOfBoundsException ex) {
			user = e.getAuthor();
		}
		
		String img = user.getEffectiveAvatarUrl() + "?size=2048";
		
		String out = "`" + user.getName() + "#" + user.getDiscriminator() + "`'s avatar is: " + img;
		
		e.sendMessage(out);
	}
	
}
