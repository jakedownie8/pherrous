package pw.naltan.pherrous.commands.pherrous.tools;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.PermUtil;

public class Stats implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get some server statistics")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("", "General server statistics.")
			.addArg("channel", "Channel statistics.")
			.addArg("member", "Member statistics")
			.addArg("role", "Role statistics");
	}
	
	@Override
	public String name() {
		
		return "stats";
	}
	
	@Override
	public void cmd(Command e) {
		
		String in = e.getIndex(1);
		switch (in) {
			case "channel" :
				channel(e);
				break;
			
			case "member" :
				
				break;
			
			case "role" :
				
				break;
			
			default :
				def(e);
				break;
		}
	}
	
	private void channel(Command e) {
		
		int cNum = 0, cNSFW = 0, cRemaining = 0;
		String cNames = "";
		
		int cCT = 0;
		for (TextChannel current : e.getGuild().getTextChannels()) {
			cNum++;
			if (cCT <= 10) {
				cNames += current.getName() + "\n";
				cCT++;
			} else {
				cRemaining++;
			}
			
			if (current.isNSFW()) {
				cNSFW++;
			}
		}
		
		int vNum = 0, vRemaining = 0;
		int vCT = 0;
		String vNames = "";
		for (VoiceChannel current : e.getGuild().getVoiceChannels()) {
			vNum++;
			
			if (vCT <= 10) {
				vNames += current.getName() + "\n";
				vCT++;
			} else {
				vRemaining++;
			}
		}
		
		String out = "Total channels: " + (cNum + vNum) + "\n";
		out += "Text: " + cNum + ", " + cNSFW + " NSFW)\n";
		out += "Voice: " + vNum;
		
		String vField = "";
		String cField = "";
		if (vCT == 11) {
			vField = "\nAnd " + vRemaining + " more...";
		}
		if (cCT == 11) {
			cField = "\nAnd " + cRemaining + " more...";
		}
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.addField("Voice Channels", vNames + vField, true);
		eb.addField("Text Channels", cNames + cField, true);
		eb.setTitle("Channel Statistics");
		
		eb.setDescription(out);
		e.sendMessage(eb);
	}
	
	private void def(Command e) {
		
		int rAll = 0, rHoisted = 0;
		for (Role current : e.getGuild().getRoles()) {
			rAll++;
			if (current.isHoisted()) {
				rHoisted++;
			}
		}
		
		int tChanAll = 0, tChanLocked = 0, tChanNSFW = 0;
		for (TextChannel current : e.getGuild().getTextChannels()) {
			tChanAll++;
			if (current.isNSFW()) {
				tChanNSFW++;
			}
		}
		
		int vChanAll = 0, vChanLocked = 0;
		
		vChanAll = e.getGuild().getVoiceChannels().size();
		
		int mems = 0, admins = 0, online = 0, dnd = 0, away = 0, offline = 0, bots = 0, ops = 0;
		for (Member current : e.getGuild().getMembers()) {
			mems++;
			if (current.getPermissions().contains(Permission.ADMINISTRATOR)) {
				admins++;
			}
			if (current.getUser().isFake() || current.getUser().isBot()) {
				bots++;
			}
			if (PermUtil.isOperator(current)) {
				ops++;
			}
			switch (current.getOnlineStatus()) {
				case INVISIBLE :
					offline++;
					break;
				
				case OFFLINE :
					offline++;
					break;
				
				case ONLINE :
					online++;
					break;
				
				case IDLE :
					away++;
					break;
				
				case DO_NOT_DISTURB :
					dnd++;
					break;
				
				case UNKNOWN :
					offline++;
					break;
			}
		}
		
		String vFormat = "Voice Channels: " + vChanAll + " total, " + vChanLocked + " hidden from everyone.";
		String tFormat = "Text Channels: " + tChanAll + " total, " + tChanNSFW + " NSFW, " + tChanLocked + " hidden from everyone.";
		String rFormat = "Roles: " + rAll + " total, " + rHoisted + " hoisted.";
		String mFormat = "Members: " + mems + " members, (" + admins + " admins, " + ops + " ops, " + bots + "bots).";
		String sFormat = "Statuses: " + online + "<:online:522403501649690635> | " + dnd + "<:dnd:522403501834502153> | " + away + "<:away:522403501813268489> | " + offline
			+ "<:offline:522403501838696458>";
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("General stats for " + e.getGuild().getName()).setDescription(vFormat + "\n" + tFormat + "\n" + rFormat + "\n" + mFormat + "\n" + sFormat);
		e.sendMessage(eb);
		
		;
		
		if (e.getGuild().getIconId() != null) {
			eb.setThumbnail(e.getGuild().getIconUrl());
		}
	}
}
