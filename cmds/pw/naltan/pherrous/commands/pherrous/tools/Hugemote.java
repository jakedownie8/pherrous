package pw.naltan.pherrous.commands.pherrous.tools;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Hugemote implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get a large version of a server emote").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "hugemote";
	}
	
	@Override
	public void cmd(Command e) {
		
		boolean failed = true;
		
		EmbedBuilder eb = new EmbedBuilder();
		if (!e.getMessage().getEmotes().isEmpty()) {
			
			eb.setImage(e.getMessage().getEmotes().get(0).getImageUrl());
			failed = false;
			e.sendMessage(eb);
			
		} else if (failed) {
			e.sendMessage("Enter a server emote");
		}
		
	}
	
}
