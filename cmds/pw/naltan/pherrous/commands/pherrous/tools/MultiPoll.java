package pw.naltan.pherrous.commands.pherrous.tools;

import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class MultiPoll implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Make a multi-option poll").setAuthor("Nathan").setVersion("1.0").addArg("<int>", "Have a poll with <int> options. Min of 2, max of 10.");
	}
	
	@Override
	public String name() {
		
		return "multipoll";
	}
	
	@Override
	public void cmd(Command e) {
		
		int number = -1;
		try {
			number = Integer.parseInt(e.getMessage().getContentRaw().split(" ")[1]);
			if (number > 10 || number < 2) {
				throw new Exception();
			}
		} catch (Exception ex) {
			e.sendMessage("Invalid syntax. Check the manual for help");
			return;
		}
		
		String[] reactions = new String[]{
			"1⃣",
			"2⃣",
			"3⃣",
			"4⃣",
			"5⃣",
			"6⃣",
			"7⃣",
			"8⃣",
			"9⃣",
			"🔟"
		};
		//4⃣ 5⃣ 6⃣ 7⃣ 8⃣ 9⃣ 🔟
		
		for (int i = 0; i != number; i++) {
			e.getMessage().addReaction(reactions[i]).queue();
		}
		
	}
	
}
