package pw.naltan.pherrous.commands.pherrous.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.Guild;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Weather implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get the weather of a location. Via wttr.in")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("", "Get the weather based off of the guild's location")
			.addArg("<city>", "Get the weather in a city/country/provence/territory");
	}
	
	@Override
	public String name() {
		
		return "weather";
	}
	
	@Override
	public void cmd(Command e) {
		
		e.getChannel().sendMessage("Processing...").queue(cons -> {
			String option = e.getRestOfMessage(1);
			
			if (option.equals("")) {
				option = serverLocation(e.getGuild());
			}
			String msg = "Weather provided by wttr.in. https://wttr.in\n\n```" + getWeather(option, e) + "```";
			msg = msg.replace("Weather report: " + option + "\n\n", "");
			
			e.sendMessage(msg);
			cons.delete().queue();
		});
		
	}
	
	private final String START = "                                                       ┌─────────────┐                                                       ";
	
	private final String END = "└──────────────────────────────┴──────────────────────────────┴──────────────────────────────┴──────────────────────────────┘";
	
	private String getWeather(String location, Command e) {
		
		try {
			String full = "```";
			URL url = new URL("http://wttr.in/~" + location);
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
				for (String line; (line = reader.readLine()) != null;) {
					full += line + "\n";
					
				}
			}
			
			full = breakFormatting(full);
			
			for (int i = 0; i < 2; i++) {
				String day = full.substring(full.lastIndexOf(START), full.lastIndexOf(END) + END.length());
				full = full.replace(day, "");
			}
			
			return full;
		} catch (MalformedURLException | UnsupportedEncodingException ex) {
			return "The URL was invalid. Did you enter the location correctly?";
		} catch (IOException ex) {
			return "An error occured, please try again later.";
		}
	}
	
	private String breakFormatting(String in) {
		
		String head = in.substring(in.indexOf("<head>"), in.indexOf("</head>") + 7);
		in = in.replace(head, "");
		
		while (in.contains("<span")) {
			int start = in.indexOf("<span");
			int end = in.indexOf(">", start) + 1;
			in = in.replace(in.substring(start, end), "");
		}
		in = in.replace("</span>", "");
		in = in.replace("&quot;", "\"");
		in = in.substring(in.indexOf("<pre>") + 5, in.indexOf("</pre>"));
		
		return in;
	}
	
	private String serverLocation(Guild g) {
		
		Region r = g.getRegion();
		/*
		 * Amsterdam
		 * Brazil
		 * EU central
		 * EU west
		 * FRankfurt
		 * Hong Kong
		 * Japan
		 * London
		 * Russia
		 * Singapore
		 * South Africa
		 * Sydney
		 * US east
		 * US central
		 * US west
		 * 
		 * 
		 */
		switch (r) {
			case AMSTERDAM :
				return "Amsterdam";
			
			case BRAZIL :
				return "Brasilia";
			
			case EU_CENTRAL :
				return "Budapest";
			
			case EU_WEST :
				return "Paris";
			
			case FRANKFURT :
				return "Frankfurt";
			
			case HONG_KONG :
				return "HongKong";
			
			case JAPAN :
				return "Tokyo";
			
			case LONDON :
				return "London";
			
			case RUSSIA :
				return "Moscow";
			
			case SINGAPORE :
				return "Singapore";
			
			case SOUTH_AFRICA :
				return "Bloemfontein";
			
			case SYDNEY :
				return "Sydney";
			
			case US_EAST :
				return "NewYork";
			
			case US_CENTRAL :
				return "Chicago";
			
			case US_WEST :
				return "SanFrancisco";
			
			default :
				return "NewYork";
		}
		
	}
	
}
