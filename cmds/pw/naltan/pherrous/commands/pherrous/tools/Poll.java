package pw.naltan.pherrous.commands.pherrous.tools;

import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Poll implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Hold a poll. Adds :thumbsup:, :thumbsdown:, and :thinking: to the message").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "poll";
	}
	
	@Override
	public void cmd(Command e) {
		
		e.addReaction("👍"); //thumbsup
		e.addReaction("🤔"); //thinking
		e.addReaction("👎"); //thumbsdown
	}
	
}
