package pw.naltan.pherrous.commands.pherrous.tools;

import java.time.format.DateTimeFormatter;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Server implements GuildCommand, Manual, Indexable {
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss");
	
	@Override
	public String category() {
		
		return "tools";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get information on the server").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "Server";
	}
	
	@Override
	public void cmd(Command e) {
		
		Guild g = e.getGuild();
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(g.getName());
		eb.setDescription("**ID**: " + g.getId() + "\n**Owner**: " + g.getOwner().getUser().getAsMention() + "\n**Region**: " + g.getRegionRaw() + "\n**Creation Date**: "
			+ g.getTimeCreated().format(dtf) + "\n**Members**: " + g.getMembers().size() + "\n**Verification Level**: " + g.getVerificationLevel() + "\n**Default Channel**: "
			+ g.getDefaultChannel().getName());
		
		String textchans = "";
		int textloop = 0, textmore = g.getTextChannels().size();
		for (TextChannel current : g.getTextChannels()) {
			textchans += "-" + current.getName() + "\n";
			textloop++;
			textmore--;
			if (textmore == 0) {
				break;
			}
			if (textloop == 5) {
				textchans += "And " + textmore + " more...";
				break;
			}
		}
		eb.addField("Text Channels (" + g.getTextChannels().size() + ")", textchans, true);
		
		String voicechans = "";
		int voiceloop = 0, voicemore = g.getVoiceChannels().size();
		for (VoiceChannel current : g.getVoiceChannels()) {
			voicechans += "-" + current.getName() + "\n";
			voiceloop++;
			voicemore--;
			if (voicemore == 0) {
				break;
			}
			if (voiceloop == 5) {
				voicechans += "And " + voicemore + " more...";
				break;
			}
		}
		eb.addField("Voice Channels (" + g.getVoiceChannels().size() + ")", voicechans, true);
		
		String roles = "";
		int roleloop = 0, rolemore = g.getRoles().size();
		for (Role current : g.getRoles()) {
			roles += "-" + current.getName() + "\n";
			roleloop++;
			rolemore--;
			if (rolemore == 0) {
				break;
			}
			if (roleloop == 5) {
				roles += "And " + rolemore + " more...";
				break;
			}
		}
		eb.addField("Roles (" + g.getRoles().size() + ")", roles, true);
		
		if (g.getIconUrl() != null) {
			eb.setThumbnail(g.getIconUrl());
		}
		e.sendMessage(eb);
	}
}
