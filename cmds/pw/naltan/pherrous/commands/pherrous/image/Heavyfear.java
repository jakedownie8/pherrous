package pw.naltan.pherrous.commands.pherrous.image;

import java.io.File;

import pw.naltan.pherrous.commands.lib.image.ImageUtil;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Heavyfear implements GuildCommand, Manual, Indexable, Assets {
	
	@Override
	public String assetDir() {
		
		return "heavyfear";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Heavyfear.key = key;
	}
	
	@Override
	public String category() {
		
		return "image";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		man.setDescription("Get a comic of Heavy saying \" I fear no man. But that thing... it scares me\" with a custom image");
		man.setAuthor("Nathan");
		man.setVersion("1.0");
		return man;
	}
	
	@Override
	public String name() {
		
		return "heavyfear";
	}
	
	@Override
	public void cmd(Command e) {
		
		e.getChannel().sendTyping().complete();
		File tofear = ImageUtil.getLastEffectiveImage(e.getMessage(), key.getDir() + "fearimg");
		if (tofear == null) {
			e.sendMessage("Image not found.");
			return;
		}
		File send = new File(key.getDir() + "out.png");
		ImageUtil.overlay(tofear.getAbsolutePath(), key.getDir() + "bg.jpg", send, 249, 441, 220, 220);
		tofear.delete();
		e.getChannel().sendFile(send, send.getName()).queue();
	}
}
