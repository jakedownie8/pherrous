package pw.naltan.pherrous.commands.pherrous.fun;

import java.util.HashMap;
import java.util.Random;

import org.json.JSONObject;

import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.PermUtil;

public class Joke implements GuildCommand, Indexable, Manual, Assets {
	
	@Override
	public String assetDir() {
		
		return "joke";
	}
	
	private static AssetKey key;
	
	private static HashMap<String, String> jokes;
	
	@SuppressWarnings("unchecked")
	@Override
	public void key(AssetKey key) {
		
		JSONObject obj = new JSONObject();
		Joke.key = key;
		obj = key.readJSON("jokes");
		jokes = new HashMap<String, String>();
		
		if (obj.isEmpty()) {
			
			obj = new JSONObject();
			
			jokes.put("Why did the chicken cross the road?", "Why not?");
			obj.put("jokes", jokes);
			updateHash();
			return;
		}
		
		jokes = (HashMap<String, String>) obj.get("jokes");
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get a joke")
			.addArg("", "Get a random joke")
			.addArg("add \"<question>\" \"<punchline\"", "Add a joke. Operator+ Only.")
			.addArg("remove \"<question>\"", "Remove a joke Operator+ Only.")
			.addArg("get", "Get pm'd a list of the jokes. Operator+ Only.")
			.setAuthor("Nathan")
			.setVersion("1.0");
	}
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public String name() {
		
		return "joke";
	}
	
	@Override
	public void cmd(Command e) {
		
		String in = "";
		
		try {
			in = e.getMessage().getContentDisplay().toLowerCase().split(" ")[1];
		} catch (Exception ex) {}
		
		switch (in) {
			
			case "add" :
				addJoke(e);
				return;
			
			case "remove" :
				rmJoke(e);
				return;
			
			case "get" :
				getJokes(e);
				return;
			
			default :
				getJoke(e);
				return;
		}
	}
	
	private void getJokes(Command e) {
		
		if (!operatorCheck(e)) {
			return;
		}
		e.sendMessage("Check your DMs!");
		e.getAuthor().openPrivateChannel().queue(channel -> {
			channel.sendFile(key.getFile("jokes.json")).queue();;
		});
	}
	
	private static String[] parseJoke(String in) {
		
		String[] out = new String[2];
		int[] jokepos = new int[4];
		try {
			
			jokepos[0] = in.indexOf("\"");
			for (int i = 1; i <= 3; i++) {
				jokepos[i] = in.indexOf("\"", jokepos[i - 1] + 1);
			}
			out[0] = in.substring(jokepos[0] + 1, jokepos[1]);
			out[1] = in.substring(jokepos[2] + 1, jokepos[3]);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return out;
	}
	
	private void updateHash() {
		
		// int json object and put jokes in
		JSONObject obj = new JSONObject();
		obj.put("jokes", jokes);
		
		// write it to the json
		key.writeJSON("jokes", obj);
	}
	
	private boolean operatorCheck(Command e) {
		
		// if they're not an operator
		if (!PermUtil.isOperator(e.getAuthor())) {
			// say they're not
			e.sendMessage("Only operators+ can run this command.");
			return false;
		}
		return true;
	}
	
	private void addJoke(Command e) {
		
		// if the user's not an op, return
		if (!operatorCheck(e)) {
			return;
		}
		
		// joke = the parsed joke
		String[] joke = parseJoke(e.getMessage().getContentRaw());
		
		// in case its null, say it didnt work and return.
		if (joke == null) {
			e.sendMessage("Invalid syntax. Check the manual for proper syntax.");
			return;
		}
		
		// add it to the hash
		jokes.put(joke[0], joke[1]);
		
		// output that it succeeded
		e.sendMessage("Added the following joke:\n" + joke[0] + "\n" + joke[1]);
		
		// update the hash
		updateHash();
	}
	
	private void rmJoke(Command e) {
		
		// if they're not an operator, stop the command
		if (!operatorCheck(e)) {
			return;
		}
		// del = the parsed joke
		String[] del = parseJoke(e.getMessage().getContentRaw());
		
		// remove it, success depends on if it actaully worked
		boolean success = jokes.remove(del[0], jokes.get(del[0]));
		
		// declaration of it as if it failed
		String out = "Removal failed. Does that joke exist?";
		
		// if it worked say it worked
		if (success) {
			out = "Removal successful.";
		}
		
		// update the hash and output the success
		updateHash();
		e.sendMessage(out);
	}
	
	private void getJoke(Command e) {
		
		// inc is an incrementer, chooser is a random joke index.
		int inc = 0;
		int size = jokes.keySet().size();
		int chooser = new Random().nextInt(size); // jokes.size();
		
		// for each joke loaded...
		for (Object joke : jokes.keySet()) {
			
			// if this is the random number...
			if (inc == chooser) {
				
				// "Why'd the chicken cross the road?"
				e.sendMessage((String) joke);
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {}
				
				// "hell if I know..."
				e.sendMessage(jokes.get(joke));
				
				// finish the command.
				return;
			}
			// if this isnt the random number, increment the counter and try
			// again.
			inc++;
		}
	}
}
