package pw.naltan.pherrous.commands.pherrous.fun;

import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class OwOify implements GuildCommand, Indexable, Manual {
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Turn text into degenerate shit").setAuthor("Nathan").setVersion("1.0");
	}
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public String name() {
		
		return "owoify";
	}
	
	@Override
	public void cmd(Command e) {
		
		//check the the command actually has some args
		if (!e.getMessage().getContentDisplay().contains(" ")) {
			e.sendMessage("Enter a message to OwOify");
			return;
		}
		
		String[][] replace = {
			{
				"ot",
				"awt"
			},
			
			{
				" er",
				" w"
			},
			{
				"r",
				"w"
			},
			{
				" o",
				" u",
			},
			{
				"w ",
				"a "
			},
			{
				"the",
				"da"
			},
			{
				"ck",
				"k"
			},
			{
				"pa",
				"paw"
			},
			{
				"o",
				"owo"
			}
		};
		
		String[] cancer = {
			"owo",
			"uwu",
			":3",
			"x3",
			"xD"
		};
		
		String msg = "";
		
		for (String current : e.getRestOfMessage(1).split(" ")) {
			
			current = " " + current + " ";
			for (String[] reparr : replace) {
				current = current.replace(reparr[0], reparr[1]);
			}
			msg += current.trim() + " ";
		}
		msg += cancer[((int) (Math.random() * cancer.length))];
		
		e.sendMessage(msg);
		
	}
	
}
