package pw.naltan.pherrous.commands.pherrous.fun;

import java.util.Random;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Minesweeper implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man
			
			.setDescription("Get a minesweeper game")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("<easy/medium/hard>", "Get an easy, medium, or hard game")
		//.addArg("custom X Y M", "Get a custom X-by-Y sized board with M mines. Max of (x*y)/2 mines.")
		
		;
	}
	
	@Override
	public String name() {
		
		return "minesweeper";
	}
	
	@Override
	public void cmd(Command e) {
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("Minesweeper");
		
		int[] sweeper = null;
		String diff = "";
		
		switch (e.getIndex(1).toLowerCase()) {
			case "easy" :
				sweeper = new int[]{
					10,
					10,
					15
				};
				diff = "Easy";
				break;
			
			case "medium" :
				sweeper = new int[]{
					13,
					13,
					30
				};
				diff = "Medium";
				break;
			
			case "hard" :
				sweeper = new int[]{
					13,
					13,
					50
				};
				diff = "Hard";
				break;
			
			default :
				sweeper = new int[]{
					10,
					10,
					15
				};
				diff = "Easy";
		}
		
		String[][] board = generateBoard(sweeper[0], sweeper[1], sweeper[2], eb, diff);
		
		String out = "";
		for (String[] row : board) {
			for (String current : row) {
				out += "||" + current + "||";
			}
			out += "\n";
		}
		
		eb.setDescription(out);
		
		e.sendMessage(eb);
		
	}
	
	private String[][] generateBoard(int x, int y, int mines, EmbedBuilder eb, String diff) {
		
		//input sanitation is fun
		x = Math.abs(x);
		y = Math.abs(y);
		mines = Math.abs(mines);
		
		//init the board and looping var
		String[][] board = new String[y][x];
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = "";
			}
		}
		
		if (mines > (x * y) / 2) {
			mines = (x * y) / 2;
		}
		int loops = mines;
		
		eb.setFooter("Difficulty: " + diff + ", " + x + "*" + y + " board, " + mines + " mines", null);
		
		Random r = new Random();
		
		do {
			int selx = r.nextInt(x);
			int sely = r.nextInt(y);
			if (board[sely][selx].equals("")) {
				board[sely][selx] = "💣";
				loops--;
			} else {}
		} while (loops > 0);
		
		for (int y1 = 0; y1 < board.length; y1++) {
			for (int x1 = 0; x1 < board[y1].length; x1++) {
				if (!board[y1][x1].equals("💣")) {
					String rep = "";
					
					int check = getNeighboring(x1, y1, board);
					switch (check) {
						case 0 :
							rep = "⬜";
							break;
						
						default :
							rep = check + "⃣";
							
					}
					
					board[y1][x1] = rep;
				} else {}
			}
		}
		
		return board;
	}
	
	/*
	 * (x-1, y-1)	(x, y-1)	(x+1, y-1)
	 * (x-1, y)		(x, y)		(x+1, y)
	 * (x-1, y+1)	(x, y+1)	(x+1, y+1)
	 */
	
	private int getNeighboring(int x, int y, String[][] arr) {
		
		int num = 0;
		
		int[][] vals = {
			{
				x - 1,
				y - 1
			},
			{
				x,
				y - 1
			},
			{
				x + 1,
				y - 1
			},
			{
				x - 1,
				y
			},
			{
				x + 1,
				y
			},
			{
				x - 1,
				y + 1
			},
			{
				x,
				y + 1
			},
			{
				x + 1,
				y + 1
			}
			
		};
		
		for (int[] current : vals) {
			try {
				if (arr[current[1]][current[0]].equals("💣")) {
					num++;
				}
			} catch (Exception ex) {}
		}
		return num;
	}
}
