package pw.naltan.pherrous.commands.pherrous.fun;

import org.json.JSONException;
import org.json.JSONObject;

import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.PermUtil;

public class T implements GuildCommand, Manual, Indexable, Assets {
	
	@Override
	public String assetDir() {
		
		return "t";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		T.key = key;
	}
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Add or get a tag.")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("<tag>", "Get a tag by the name <tag>")
			.addArg("remove", "Remove a tag. Operator Only.")
			.addArg("add <tag> <text>", "Add <text> to a new tag <tag>. Does not override other tags.");
	}
	
	@Override
	public String name() {
		
		return "t";
	}
	
	@Override
	public void cmd(Command e) {
		
		String in = e.getIndex(1);
		switch (in) {
			case "add" :
				add(e);
				break;
			
			case "remove" :
				remove(e);
				break;
			
			default :
				get(e);
		}
	}
	
	private void get(Command e) {
		
		JSONObject obj = new JSONObject();
		obj = key.readJSON("tags");
		String option = "";
		try {
			option = e.getMessage().getContentRaw().toLowerCase().split(" ")[1];
		} catch (Exception ex) {
			e.sendMessage("Please enter a tag");
			return;
		}
		try {
			e.sendMessage((String) obj.get(option));
		} catch (JSONException ex) {
			e.sendMessage("Tag doesn't exist.");
		}
		
	}
	
	private void remove(Command e) {
		
		if (!PermUtil.isOperator(e.getMember())) {
			e.sendMessage("You do not have permission to use this command.");
			return;
		}
		JSONObject obj = new JSONObject();
		obj = key.readJSON("tags");
		String remove = "";
		try {
			remove = e.getMessage().getContentRaw().toLowerCase().split(" ")[2];
		} catch (Exception ex) {
			e.sendMessage("Enter a tag to remove");
			return;
		}
		Object success = obj.remove(remove);
		String out = "Removal failed. The tag probably doesn't exist.";
		if (success != null) {
			out = "Removal successful.";
		}
		e.sendMessage(out);
	}
	
	private void add(Command e) {
		
		String tag = "", desc = "";
		JSONObject obj = new JSONObject();
		obj = key.readJSON("tags");
		try {
			tag = e.getMessage().getContentRaw().split(" ")[2];
			desc = e.getRestOfMessage(3);
			
			for (Object current : obj.keySet()) {
				if (current.toString().toLowerCase().equals(tag)) {
					throw new Exception();
				}
			}
		} catch (Exception ex) {
			e.sendMessage("Invalid input. Check the manual for help.");
			return;
		}
		obj.put(tag, desc);
		key.writeJSON("tags", obj);
		e.sendMessage("Added tag " + tag + " with text " + desc);
	}
	
}
