package pw.naltan.pherrous.commands.pherrous.fun;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Webhook;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Mimic implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Mimic a user, or yourself is no user is specified")
			.setAuthor("Naltan")
			.setVersion("1.0")
			.addArg("<@user> im a nerd", "Makes a bot say as the user fthat they're a nerd");
	}
	
	@Override
	public String name() {
		
		return "mimic";
	}
	
	@Override
	public void cmd(Command e) {
		
		e.getChannel().sendTyping().queue();
		Member member = null;
		int startindex = 2;
		try {
			member = e.getMessage().getMentionedMembers().get(0);
		} catch (IndexOutOfBoundsException ex) {
			member = e.getMember();
			startindex = 1;
		}
		
		String out = "";
		try {
			out = e.getRestOfMessage(startindex);
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("Please enter a message");
			return;
		}
		
		try {
			
			Webhook hook = e.getChannel().createWebhook("Pherrous-mimic").complete(true);
			e.getChannel().retrieveWebhooks().complete();
			
			WebhookMessageBuilder wb = new WebhookMessageBuilder();
			wb.setContent(out);
			wb.setUsername(member.getEffectiveName());
			wb.setAvatarUrl(member.getUser().getEffectiveAvatarUrl());
			
			WebhookClient client = WebhookClient.withUrl(hook.getUrl());
			client.send(wb.build()).thenAccept(cons -> {
				hook.delete().queue();
			});
			
		} catch (RateLimitedException ex) {
			e.sendMessage("I'm being rate limited. Try again later.");
		}
	}
}
