package pw.naltan.pherrous.commands.pherrous.fun;

import java.util.Random;

import net.dv8tion.jda.api.EmbedBuilder;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Magic8Ball implements GuildCommand, Manual, Indexable {
	
	static Random r = new Random();
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Ask a yes or no question to the magic 8 ball").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "magic8ball";
	}
	
	@Override
	public void cmd(Command e) {
		
		String[] answers = new String[]{
			"It is certain", // Affirmative
			"It is decidedly so",
			"Without a doubt",
			"Yes, definetley",
			"You may rely on it",
			"As I see it, yes",
			"Most likely",
			"Outlook good",
			"Yes",
			"Signs point to yes",
			"Reply hazy, try again", // Neutral
			"Ask again later",
			"Better not tell you now...",
			"Cannot predict now",
			"Concentrate and ask again",
			"Dont count on it...", // Negative
			"My reply is no",
			"My sources say no",
			"Outlook not so good",
			"Very doubtful"
		};
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("The magic 8 Ball says...");
		eb.setDescription(answers[r.nextInt(20)]);
		e.sendMessage(eb);
	}
}
