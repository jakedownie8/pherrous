package pw.naltan.pherrous.commands.pherrous.fun;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Bewareof implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Creates a message to \"Beware of X or they will Y\"").setAuthor("Naltan").setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "bewareof";
	}
	
	@Override
	public void cmd(Command e) {
		
		Member beware = null;
		int startIndex = 2; // assumes the user is mentioned
		
		try {
			beware = e.getMessage().getMentionedMembers().get(0);
		} catch (IndexOutOfBoundsException ex) {
			beware = e.getMember();
			startIndex = 1; // if they aren't, then bumps the index down by one
		}
		
		// now to calculate the actual start of the message
		String bewarename = beware.getEffectiveName();
		
		if (bewarename.contains(" ")) { // if the name contains a space...
			while (bewarename.contains(" ")) { // ...and while it does...
				bewarename = bewarename.replaceFirst(" ", ""); //replace the first occurence with nothing...
				startIndex++; // ... and increment the start index
			}
		}
		
		String message = e.getRestOfMessage(startIndex);
		
		if (message.equals("")) {
			e.sendMessage("Enter a message");
			return;
		}
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("BEWARE OF " + beware.getEffectiveName().toUpperCase())
			.setDescription("Just a fair warning, peeps. You wanna be safe in this day and age!" + "\n" + "\n" + "Look out for a Discord user by the name of \""
				+ beware.getEffectiveName() + "\" with the tag #" + beware.getUser().getDiscriminator()
				+ ". They are going around sending friend requests to random Discord users, and those who accept this person's friend request will " + message
				+ ". Spread the word and send this to as many discord servers as you can. If you see this user, DO NOT accept their friend requests and immediately block them."
				+ "\n" + "\n" + "Our team is currently working very hard to remove this user from our database, please stay safe." + "\n" + "\n" + "-The Discord team")
			.setThumbnail(beware.getUser().getEffectiveAvatarUrl());
		
		e.sendMessage(eb);
	}
	
}
