package pw.naltan.pherrous.commands.pherrous.fun;

//TODO: Add something that sorts the files alphabetically. Windows does this automagically, but Linux doesn't.
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message.Attachment;
import pw.naltan.pherrous.core.output.Menu;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.settings.Settings;
import pw.naltan.pherrous.util.PermUtil;
import pw.naltan.pherrous.util.StrUtil;

public class Mp3 implements GuildCommand, Manual, Indexable, Assets {
	
	// ----- BEGIN STATIC DECLERATIONS
	
	private static AssetKey key;
	private static List<File> files = new ArrayList<File>();
	
	// ----- END STATIC DECLERATIONS
	// ----- BEGINE PHERROUS META
	
	@Override
	public String category() {
		
		return "fun";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Get an mp3 file")
			.setAuthor("Naltan, Snoipah")
			.setVersion("1.0")
			.addArg("random", "Get a random MP3 file")
			.addArg("list", "Get a list of available MP3 files")
			.addArg("get <number>", "Get a select MP3 file from the list")
			.addArg("manage <add/remove/size>", "Manage the MP3s currently in Pherrous. Operators+ only.");
	}
	
	@Override
	public String name() {
		
		return "mp3";
	}
	
	@Override
	public void key(AssetKey key) {
		
		initFiles(key);
	}
	
	private static void initFiles(AssetKey key) {
		
		Mp3.key = key;
		File[] files = new File(key.getDir()).listFiles();
		for (File current : files) {
			
			if (current.length() > 8000000) {
				System.out.println("[MP3] Deleted \"" + current.getName() + "\" for being larger than 8MB");
				current.delete();
				
			}
			if (!current.getName().toLowerCase().endsWith(".mp3") && current.exists()) {
				System.out.println("[MP3] Deleted\"" + current.getName() + "\" for being an incompatibile file");
				current.delete();
			}
			if (current.length() <= 8000000 && current.exists()) {
				Mp3.files.add(current);
			}
			
		}
	}
	
	@Override
	public String assetDir() {
		
		return "mp3";
	}
	
	// ----- END PHERROUS META
	// ----- BEGIN COMMAND
	
	@Override
	public void cmd(Command e) {
		
		//Standard argument stuff. If they don't specify, let them know and return.
		String argument = e.getIndex(1);
		
		switch (argument) {
			case "random" : //if the user wants a random MP3
				random(e);
				return;
			
			case "list" :
				list(e);
				return;
			
			case "get" :
				get(e);
				return;
			
			case "manage" :
				manage(e);
				return;
			
			default :
				list(e);
				return;
		}
	}
	
	private static void random(Command e) {
		
		try {
			//get a random file from the list
			int random = new Random().nextInt(files.size());
			File select = files.get(random);
			//in case the length is too long, delete it from the system and file list and let the user know.
			if (select.length() >= 8000000) {
				select.delete();
				files.remove(random);
				e.sendMessage("The randomly selected file was too large and has been deleted. Please try again.");
				return;
			}
			//otherwise output the file and return.
			e.sendMessage(select);
			return;
			
		} catch (IndexOutOfBoundsException ex) {
			//this is thrown by files.get if the index doesn't exist. This should only happen if the list is empty, meaning the directory doesnt exist or the folder is empty.
			e.sendMessage("The MP3 asset folder is not present or is empty.\nAn operator needs to add some mp3s.");
			return;
		}
	}
	
	private static void list(Command e) {
		
		//returns a list of MP3s
		Menu mb = new Menu("**Files:**");
		EmbedBuilder emb = new EmbedBuilder();
		
		//add each file as an option
		for (File current : files) {
			String temp = current.getName();
			
			//make the names nicer. (foo_bar.mp3 ->  foo bar)
			temp = temp.replace(".mp3", "").replace("_", " ");
			
			//If the name is too long, cut it off and add elipses
			if (temp.length() > 32) {
				temp = temp.substring(0, 31) + "...";
			}
			
			emb.setDescription(mb.build());
			mb.clear();
			
		}
		
		if (mb.build() == "") {
			e.sendMessage("I don't have any MP3s! Tell an operator");
		} else {
			e.sendMessage(emb);
		}
		
		//create an embed, shove the menu in to it, and return.
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("Audio files:").setDescription(mb.build());
		e.sendMessage(eb);
		return;
	}
	
	private static void get(Command e) {
		
		//try to get the index that they want, if its impossible then return.
		String select = e.getIndex(2);
		
		try {
			int num = Integer.parseInt(select);
			File out = Mp3.files.get(num - 1);
			e.getChannel().sendTyping().queue();
			e.sendMessage(out);
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("MP3 with number " + select + " doesn't exist");
		} catch (NumberFormatException ex) {
			if (select.equals("")) {
				e.sendMessage("Enter the number for the MP3 you want");
			} else {
				e.sendMessage("Enter a number");
			}
		}
		return;
	}
	
	private static void manage(Command e) {
		
		switch (e.getIndex(2)) {
			case "add" :
				manageAdd(e);
				return;
			
			case "delete" :
				manageDelete(e);
				return;
			
			case "size" :
				manageSize(e);
				return;
			
			default :
				e.sendMessage("Args: " + Settings.getPrefix() + "mp3 manage size/add/delete");
				return;
		}
		
	}
	
	private static void manageAdd(Command e) {
		
		if (!PermUtil.isOperator(e.getMember())) {
			e.sendMessage("You do not have permission to use this command");
			return;
		}
		
		try {
			
			Attachment att = e.getMessage().getAttachments().get(0);
			if (!att.getFileName().toLowerCase().endsWith(".mp3")) {
				e.sendMessage("Please enter an MP3 file.");
				return;
			}
			String filename = att.getFileName().replace("_", " ");
			File f = new File(key.getDir() + filename);
			if (f.exists()) {
				e.sendMessage("The file \"" + filename + "\" already exists.");
				return;
			}
			
			if (att.getSize() > 8000000) {
				e.sendMessage("That file is too large to add. (>8MB)");
				return;
			}
			boolean success = att.downloadToFile().complete(f);
			if (success) {
				files.clear();
				initFiles(key);
				e.sendMessage("File \"" + filename + "\" successfully added to my list of MP3s");
				return;
			} else {
				e.sendMessage("An error occured while downloading that file. Please try again later.");
				return;
			}
			
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("Run the command with a file attached.");
			return;
		}
	}
	
	private static void manageDelete(Command e) {
		
		try {
			int file = Integer.parseInt(e.getRestOfMessage(3));
			
			if (file <= 0 || file > files.size()) {
				throw new NumberFormatException();
			}
			
			File delete = files.get(file - 1);
			files.remove(file - 1);
			
			String filename = delete.getName();
			
			boolean success = delete.delete();
			if (success) {
				e.sendMessage("File \"" + filename + "\" successfully deleted.");
				return;
			}
			if (!success) {
				e.sendMessage("An error occured while trying to remove that file. Please try again.");
				return;
			}
			
		} catch (ArrayIndexOutOfBoundsException | NumberFormatException ex) {
			e.sendMessage("Enter the number of the file to delete. (min 1, max " + files.size() + ").");
			return;
		}
		
	}
	
	private static void manageSize(Command e) {
		
		Menu menu = new Menu("**Files:**");
		
		for (int i = 0; i != files.size(); i++) {
			String temp = files.get(i).getName();
			
			temp = StrUtil.capitalizeFirst(temp.toLowerCase().replace(".mp3", "").replace(".wav", ""));
			
			double size = files.get(i).length() / 1000;
			
			size = Double.valueOf(new DecimalFormat("0000.0").format(size));
			
			if (temp.length() > 32) {
				temp = temp.substring(0, 31) + "...";
			}
			
			String sizestr = size + "KB";
			int spacing = sizestr.length();
			while (spacing != 12) {
				sizestr += ".";
				spacing++;
			}
			
			menu.addItem(sizestr + temp);
			if (menu.build().length() >= 2000) {
				
				EmbedBuilder emb = new EmbedBuilder();
				emb.setTitle("Audio files:").setDescription("```" + menu.build() + "```");
				e.sendMessage(emb);
				menu.clear();
			}
		}
		EmbedBuilder emb = new EmbedBuilder();
		emb.setTitle("Audio files:");
		
		String desc = "```" + menu.build() + "```";
		if (desc.equals("``````")) {
			return;
		}
		emb.setDescription(desc);
		e.sendMessage(emb);
		
		return;
	}
}
