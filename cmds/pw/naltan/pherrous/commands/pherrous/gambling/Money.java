package pw.naltan.pherrous.commands.pherrous.gambling;

import java.math.BigInteger;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import pw.naltan.pherrous.commands.lib.money.MoneyHandler;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Money implements GuildCommand, Manual, Indexable {
	
	static AssetKey key;
	
	@Override
	public String category() {
		
		return "Gambling";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man
			
			.setDescription("Get or modify your money")
			.setAuthor("Naltan")
			.setVersion("1.0")
			.addArg("", "Get your balance")
			//.addArg("leaderboard", "Get the leaderboard for your server")
			.addArg("send @User <number>", "Send funds to a user")
			.addArg("default <number>", "Set the default amount of money. Admin only.")
			.addArg("icon :emote:", "Set the emote for your server's money. Admin only.")
			.addArg("reset @User", "Reset a user's money. Admin only.")
			.addArg("resetall", "Reset everyone's money. Admin only.")
			.addArg("set @User <number>", "Set a user's money. Admin only.")
			.addArg("add @user <number>", "Add funds to a user's account. Admin Only.")
			.addArg("remove @User <number>", "Remove funds from a user's account. Admin Only");
	}
	
	@Override
	public String name() {
		
		return "money";
	}
	
	@Override
	public void cmd(Command e) {
		
		MoneyHandler m = new MoneyHandler(e.getGuild().getId());
		
		switch (e.getIndex(1)) {
			case "send" :
				send(m, e);
				break;
			
			case "default" :
				def(m, e);
				break;
			
			case "icon" :
				icon(m, e);
				break;
			
			case "reset" :
				reset(m, e);
				break;
			
			case "resetall" :
				resetAll(m, e);
				break;
			
			case "add" :
				addRemoveSet(m, e, 1);
				break;
			
			case "remove" :
				addRemoveSet(m, e, 0);
				break;
			
			case "set" :
				addRemoveSet(m, e, 2);
				break;
			
			default :
				balance(m, e);
		}
		
	}
	
	private void addRemoveSet(MoneyHandler m, Command e, int operation) {
		
		String prob1 = "", prob2 = "";
		
		try {
			String val = e.getMessage().getContentRaw().split(" ")[3];
			User usr = e.getMessage().getMentionedUsers().get(0);
			BigInteger num = new BigInteger(val);
			boolean success = false;
			switch (operation) {
				case 0 :
					success = m.deductMoney(e.getMember(), usr.getId(), num);
					prob1 = "deduct";
					prob2 = "deduct money from";
					break;
				
				case 1 :
					success = m.addMoney(e.getMember(), usr.getId(), num);
					prob1 = "add";
					prob2 = "add money to";
					break;
				
				case 2 :
					success = m.setMoney(e.getMember(), usr.getId(), num);
					prob1 = "set";
					prob2 = "set the money for";
					break;
			}
			if (!success) {
				throw new Exception();
			}
		} catch (ArrayIndexOutOfBoundsException ex) {
			e.sendMessage("Enter the amount of money to " + prob1);
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("Mention a user to " + prob2);
		} catch (NumberFormatException ex) {
			e.sendMessage("Enter a number");
		} catch (Exception ex) {
			e.sendMessage("You do not have permission to use this command.");
		}
	}
	
	private void balance(MoneyHandler m, Command e) {
		
		User target = e.getAuthor();
		BigInteger val = m.getMoney(e.getAuthor().getId());
		if (!e.getMessage().getMentionedUsers().isEmpty()) {
			target = e.getMessage().getMentionedUsers().get(0);
			val = m.getMoney(e.getMessage().getMentionedUsers().get(0).getId());
		}
		
		int bits = val.bitCount() % 8, bytes = (val.bitCount() - bits) / 8;
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(target.getName() + "'s Balance").setDescription(m.getIcon() + " `" + val.toString() + "`").setFooter(bits + " bits and " + bytes + " bytes.", null);
		e.sendMessage(eb);
	}
	
	private void send(MoneyHandler m, Command e) {
		
		try {
			Member source = e.getMember(), target = e.getMessage().getMentionedMembers().get(0);
			
			BigInteger amt = new BigInteger(e.getMessage().getContentRaw().split(" ")[3]);
			if (MoneyHandler.isNegative(amt)) {
				throw new NumberFormatException();
			}
			if (m.deductMoney(source.getUser().getId(), amt)) {
				m.addMoney(target.getUser().getId(), amt);
			}
			
			EmbedBuilder eb = new EmbedBuilder();
			eb.setTitle("Success!").setDescription("Successfully sent " + m.getIcon() + " `" + amt.toString() + "` to " + target.getAsMention());
			e.sendMessage(eb);
			
		} catch (IndexOutOfBoundsException ex) {
			e.sendMessage("Mention a user to send money to");
		} catch (NumberFormatException ex) {
			e.sendMessage("Enter an amount >0 to send");
		}
	}
	
	private void icon(MoneyHandler m, Command e) {
		
		try {
			String ico = e.getMessage().getContentRaw().split(" ")[2];
			boolean success = m.setIcon(e.getMember(), ico);
			if (!success) {
				throw new Exception();
			}
			
			e.sendMessage("Successfully set this guild's icon to " + ico);
			
		} catch (ArrayIndexOutOfBoundsException ex) {
			
			e.sendMessage("Enter the icon that will represent this guild's currency");
			
		} catch (Exception ex) {
			
			e.sendMessage("You do not have permision to use this command.");
			
		}
	}
	
	private void reset(MoneyHandler m, Command e) {
		
		try {
			if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
				throw new Exception();
			}
			
			Member reset = e.getMessage().getMentionedMembers().get(0);
			m.resetMoney(e.getMember(), reset.getUser().getId());
			
			e.sendMessage("Sucessfully reset " + reset.getAsMention() + "'s money");
			
		} catch (ArrayIndexOutOfBoundsException ex) {
			e.sendMessage("Message a user to reset the money for");
		} catch (Exception ex) {
			e.sendMessage("You do not have permission to use this command.");
		}
	}
	
	private void def(MoneyHandler m, Command e) {
		
		try {
			BigInteger num = new BigInteger(e.getMessage().getContentRaw().split(" ")[2]);
			if (MoneyHandler.isNegative(num)) {
				throw new NumberFormatException();
			}
			if (!m.setDefault(e.getMember(), num)) {
				throw new Exception();
			}
			
			e.sendMessage("Sucessfully set the guild's default money to " + num.toString());
			
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
			e.sendMessage("Enter a number to set the default to that's greater than 0.");
		} catch (Exception ex) {
			e.sendMessage("You do not have permission to use this command");
		}
	}
	
	private void resetAll(MoneyHandler m, Command e) {
		
		boolean success = m.resetAll(e.getMember());
		if (success) {
			e.sendMessage("Money reset.");
		} else {
			e.sendMessage("You don't have permission to use that command.");
		}
	}
	
}
