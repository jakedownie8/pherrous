package pw.naltan.pherrous.commands.pherrous.admin;

import java.util.ArrayList;
import java.util.List;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class PermCheck implements GuildCommand, Manual, Indexable {
	
	/*
	 * Permission flaws
	 * 
	 * -1: roles without admin can mention everyone
	 * -2: roles without admin can talk in some channels named announcements, welcome, etc
	 * -3: roles with a large portion of the server's population can be mentioned (75%+?)
	 * -4: Bot has admin
	 * 
	 * Warnings
	 * -1: More than n% of users have the admin permission (25%?)
	 * -2: TTS being on
	 * 
	 */
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public String name() {
		
		return "permcheck";
	}
	
	@Override
	public void cmd(Command e) {
		
		if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
			e.sendMessage("Only admins can run this command");
			return;
		}
		EmbedBuilder eb = new EmbedBuilder();
		
		String msg = e.getMessage().getContentRaw().toLowerCase();
		boolean nochanname = arg(msg, "--nochanname", "-c");
		boolean norolecheck = arg(msg, "--norolecheck", "-r");
		boolean nopm = arg(msg, "--nopm", "-p");
		
		if (!nochanname) {
			eb.addField("Channel Names", chanNameCheck(e), true);
		}
		if (!norolecheck) {
			eb.addField("Role Abilities", roleCheck(e), true);
		}
		if (nopm) {
			e.sendMessage(eb);
		} else {
			e.getAuthor().openPrivateChannel().queue(consumer -> {
				consumer.sendMessage(eb.build()).queue();
			});
		}
		
	}
	
	private String roleCheck(Command e) {
		
		String out = "**__Mention Everyone__**\n";
		
		//Roles
		for (Role crole : e.getGuild().getRoles()) {
			if (crole.hasPermission(Permission.MESSAGE_MENTION_EVERYONE) && !crole.hasPermission(Permission.ADMINISTRATOR)) {
				out += ":x:\"" + crole.getName() + "\" can mention everyone and is not an administrator.\n";
			}
		}
		if (out.equals("**__Mention Everyone__**\n")) {
			out += ":white_check_mark: No problems found\n\n";
		}
		
		//
		e.getGuild().getRoles().get(0);
		
		return out;
	}
	
	private String chanNameCheck(Command e) {
		
		String prob = "";
		
		String[] names = {
			"announcements",
			"welcome",
			"admin",
			"staff",
		
		};
		
		List<TextChannel> chans = new ArrayList<TextChannel>();
		for (TextChannel current : e.getGuild().getTextChannels()) {
			for (String name : names) {
				if (current.getName().toLowerCase().contains(name.toLowerCase())) {
					chans.add(current);
				}
			}
		}
		
		return prob;
		
	}
	
	private boolean arg(String message, String full, String abbrev) {
		
		return message.toLowerCase().contains(full.toLowerCase()) || message.toLowerCase().contains(abbrev.toLowerCase());
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setAuthor("Nathan")
			.setVersion("1.0")
			.setDescription("Check the permissions in your server for any potential faults. Admin Only")
			.addArg("<--nochanname OR -c>", "Exclude channel name checking. I.E. warnings about everyone being able to talk in a channel called #announcements will be omitted from the report.")
			.addArg("<--norolecheck OR -r>", "Warnings about roles with many users that allow dangerous permissions like admin or mention everyone are omitted from the report.")
			.addArg("<--nopm OR -p>", "Don't PM the report to the sender, instead send it in the channel.");
	}
	
}
