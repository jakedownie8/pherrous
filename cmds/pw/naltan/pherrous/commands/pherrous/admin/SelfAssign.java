package pw.naltan.pherrous.commands.pherrous.admin;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class SelfAssign implements GuildCommand, Indexable, Manual, Assets {
	
	@Override
	public String assetDir() {
		
		return "selfassign";
	}
	
	private static List<String> roleIds;
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		SelfAssign.key = key;
		roleIds = new ArrayList<String>();
		JSONObject obj = new JSONObject();
		obj = key.readJSON("roles");
		JSONArray arr = new JSONArray();
		try {
			arr = obj.getJSONArray("roles");
		} catch (JSONException ex) {
			return;
		}
		for (Object current : arr) {
			String temp = (String) current;
			roleIds.add(temp);
		}
	}
	
	private void writeChanges() {
		
		JSONArray arr = new JSONArray();
		for (String current : roleIds) {
			arr.put(current);
		}
		JSONObject obj = new JSONObject();
		obj.put("roles", arr);
		key.writeJSON("roles", obj);
	}
	
	private List<Role> getGuildRoles(Guild e) {
		
		List<Role> out = new ArrayList<Role>();
		for (String current : roleIds) {
			for (Role currentRole : e.getRoles()) {
				if (currentRole.getId().equals(current)) {
					out.add(currentRole);
				}
			}
		}
		return out;
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setAuthor("Nathan")
			.setVersion("1.0")
			.setDescription("Assign roles to yourself")
			.addArg("list", "List the available roles")
			.addArg("add <role number>", "Add a role to yourself")
			.addArg("remove <role number>", "Remove a role from yourself")
			.addArg("manage <add|remove> @Role", "Add or remove roles from the list of self-assignable ones. Administrator only.");
	}
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public String name() {
		
		return "selfassign";
	}
	
	@Override
	public void cmd(Command e) {
		
		String in = "";
		try {
			in = e.getMessage().getContentRaw().split(" ")[1];
		} catch (Exception ex) {}
		
		switch (in) {
			case "list" :
				list(e);
				return;
			
			case "add" :
				add(e);
				return;
			
			case "remove" :
				remove(e);
				return;
			
			case "manage" :
				
				if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
					e.sendMessage("Only Administrators can manage this.");
					return;
				}
				String in2 = "";
				try {
					in2 = e.getMessage().getContentRaw().split(" ")[2];
				} catch (Exception ex) {}
				
				switch (in2) {
					case "add" :
						manageAdd(e);
						return;
					
					case "remove" :
						manageRemove(e);
						return;
					
					default :
						e.sendMessage("Enter add/remove");
						return;
				}
				
			default :
				list(e);
		}
	}
	
	private void remove(Command e) {
		
		List<Role> guildRoles = new ArrayList<Role>();
		guildRoles = getGuildRoles(e.getGuild());
		if (guildRoles.isEmpty()) {
			e.sendMessage("This guild has no self-assignable roles. Sorry!");
			return;
		}
		int selection = 0;
		try {
			selection = Integer.parseInt(e.getMessage().getContentRaw().split(" ")[2]);
			if (selection <= 0 || selection > guildRoles.size()) {
				throw new Exception();
			}
		} catch (Exception ex) {
			e.sendMessage("Enter the number for the role you want to remove.");
			return;
		}
		
		Role get = guildRoles.get(selection - 1);
		
		if (!e.getMember().getRoles().contains(get)) {
			e.sendMessage("You don't have that role");
			return;
		}
		
		e.getGuild().removeRoleFromMember(e.getMember(), get).queue(action -> {
			e.sendMessage("Removed role " + get.getName());
		});
	}
	
	private void list(Command e) {
		
		List<Role> guildRoles = new ArrayList<Role>();
		guildRoles = getGuildRoles(e.getGuild());
		if (guildRoles.isEmpty()) {
			e.sendMessage("This guild has no self-assignable roles. Sorry!");
			return;
		}
		
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(e.getGuild().getName() + "'s Roles");
		
		String out = "Here's a list of my self-assignable roles: ```";
		int count = 0;
		for (Role current : guildRoles) {
			count++;
			out += "\n" + count + ": " + current.getName();
		}
		out += "```";
		eb.setDescription(out);
		e.sendMessage(eb);
	}
	
	private void add(Command e) {
		
		List<Role> guildRoles = new ArrayList<Role>();
		guildRoles = getGuildRoles(e.getGuild());
		if (guildRoles.isEmpty()) {
			e.sendMessage("This guild has no self-assignable roles. Sorry!");
			return;
		}
		int selection = 0;
		try {
			selection = Integer.parseInt(e.getMessage().getContentRaw().split(" ")[2]);
			if (selection <= 0 || selection > guildRoles.size()) {
				throw new Exception();
			}
		} catch (Exception ex) {
			e.sendMessage("Enter the number for the role you want.");
			return;
		}
		
		Role get = guildRoles.get(selection - 1);
		
		if (e.getMember().getRoles().contains(get)) {
			e.sendMessage("You already have that role");
			return;
		}
		
		e.getGuild().addRoleToMember(e.getMember(), get).queue(action -> {
			e.sendMessage("Added role " + get.getName());
		});
	}
	
	private void manageAdd(Command e) {
		
		List<Role> roles = e.getMessage().getMentionedRoles();
		
		if (roles.isEmpty()) {
			e.sendMessage("Mention roles to add");
			return;
		}
		
		String out = "Added the following role(s):";
		for (Role current : roles) {
			roleIds.add(current.getId());
			out += "\n" + current.getName();
		}
		writeChanges();
		e.sendMessage(out);
	}
	
	private void manageRemove(Command e) {
		
		List<Role> roles = e.getMessage().getMentionedRoles();
		if (roles.isEmpty()) {
			e.sendMessage("Mention role(s) to remove");
			return;
		}
		String out = "```";
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("Role Removal").setFooter("\"-\" is successful, \"!\" is unsuccessful.", null);
		for (Role current : roles) {
			boolean success = roleIds.remove(current.getId());
			if (!success) {
				out += "\n[!] " + current.getName();
			}
			if (success) {
				out += "\n[-] " + current.getName();
			}
		}
		eb.setDescription(out + "```");
		writeChanges();
		e.sendMessage(eb);
	}
}
