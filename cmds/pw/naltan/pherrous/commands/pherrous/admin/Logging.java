package pw.naltan.pherrous.commands.pherrous.admin;

import org.json.JSONObject;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;
import pw.naltan.pherrous.util.StrUtil;

public class Logging extends ListenerAdapter implements GuildCommand, Manual, Indexable, Assets, Passthrough {
	
	// START EVENTS
	
	@Override
	public void onGuildMemberJoin(GuildMemberJoinEvent e) {
		
		System.out.println(e.getGuild().getId());
		JSONObject obj = key.readJSON("join-" + e.getGuild().getId());
		if (obj.isEmpty()) {
			return;
		}
		for (Object currentObj : obj.keySet()) {
			String current = (String) currentObj;
			TextChannel out = e.getGuild().getTextChannelById(current);
			String outmsg = (String) obj.get(current);
			
			outmsg = outmsg.replace("%user%", e.getUser().getAsMention())
				.replace("%id%", e.getUser().getId())
				.replace("%discriminator%", e.getUser().getDiscriminator())
				.replace("%username%", e.getUser().getName())
				.replace("%guild%", e.getGuild().getName());
			try { // if the channel is deleted, this catches potential null
					// pointers
				out.sendMessage(outmsg).queue();
			} catch (Exception ex) {}
		}
	}
	
	@Override
	public void onGuildMemberLeave(GuildMemberLeaveEvent e) {
		
		JSONObject obj = key.readJSON("leave-" + e.getGuild().getId());
		if (obj.isEmpty()) {
			return;
		}
		for (Object currentObj : obj.keySet()) {
			String current = (String) currentObj;
			TextChannel out = e.getGuild().getTextChannelById(current);
			String outmsg = (String) obj.get(current);
			
			outmsg = outmsg.replace("%user%", e.getUser().getAsMention())
				.replace("%id%", e.getUser().getId())
				.replace("%discriminator%", e.getUser().getDiscriminator())
				.replace("%username%", e.getUser().getName())
				.replace("%guild%", e.getGuild().getName());
			
			try { // if the channel is deleted, this catches potential null
					// pointers
				out.sendMessage(outmsg).queue();
			} catch (Exception ex) {}
		}
	}
	
	// END EVENTS
	// START INTERFACE
	
	@Override
	public String assetDir() {
		
		return "logging";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		Logging.key = key;
	}
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setAuthor("Nathan")
			.setVersion("1.0")
			.setDescription("Enable or disable some logging. Admin+ Only.")
			.addArg("join", "Edit aspects of join logging.")
			.addArg("leave", "Edit aspects of leave logging.");
	}
	
	@Override
	public String name() {
		
		return "logging";
	}
	
	// END INTERFACE
	// START GENERAL SWITCH
	
	@Override
	public void cmd(Command e) {
		
		if (!e.getMember().getPermissions().contains(Permission.ADMINISTRATOR)) {
			e.sendMessage("You don't have permission to use this command.");
			return;
		}
		switch (e.getIndex(1)) {
			case "join" :
				join(e);
				return;
			
			case "leave" :
				leave(e);
				return;
			
			default :
				e.sendManual(man(new ManualBuilder("Logging")));
		}
	}
	
	// END GENERAL SWITCH
	// START JOIN/LEAVE
	
	private void join(Command e) {
		
		switch (e.getIndex(2)) {
			case "enable" :
				joinLeaveEnable(e, "join", "joined");
				return;
			
			case "disable" :
				joinLeaveDisable(e, "join");
				return;
			
			case "status" :
				joinLeaveStatus(e, "join");
				return;
			
			case "message" :
				joinLeaveMessage(e, "join");
				return;
			
			default :
				joinLeaveManual(e, "join", "joined");
		}
	}
	
	private void leave(Command e) {
		
		String in = "";
		try {
			in = e.getMessage().getContentRaw().split(" ")[2];
		} catch (Exception ex) {}
		
		switch (in) {
			case "enable" :
				joinLeaveEnable(e, "leave", "left");
				return;
			
			case "disable" :
				joinLeaveDisable(e, "leave");
				return;
			
			case "status" :
				joinLeaveStatus(e, "leave");
				return;
			
			case "message" :
				joinLeaveMessage(e, "leave");
				return;
			
			default :
				joinLeaveManual(e, "leave", "left");
		}
	}
	
	private void joinLeaveEnable(Command e, String present, String past) {
		
		present = StrUtil.capitalizeFirst(present);
		past = past.toLowerCase();
		
		JSONObject obj = getObj(e, present.toLowerCase());
		if (obj.has(e.getChannel().getId())) {
			e.sendMessage(present + " notifications are already enabled here");
			return;
		}
		obj.put(e.getChannel().getId(), "%user% has " + past + " %guild%");
		e.sendMessage(present + " notifications enabled for this channel.\nThe default message is `%user% has " + past + " %guild%`, "
			+ "check the manual for instructions on how to change it.");
		writeObj(e, present.toLowerCase(), obj);
	}
	
	private void joinLeaveDisable(Command e, String present) {
		
		present = present.toLowerCase();
		
		JSONObject obj = getObj(e, "join");
		
		try {
			obj.get(e.getChannel().getId()).equals(null); // should throw the
															// null pointer
			obj.remove(e.getChannel().getId());
			writeObj(e, present, obj);
			e.sendMessage("Successfully removed " + present + " messages in this channel.");
			return;
		} catch (NullPointerException ex) {
			e.sendMessage(StrUtil.capitalizeFirst(present) + " notifications were not enabled for this channel.");
			return;
		}
	}
	
	private void joinLeaveStatus(Command e, String present) {
		
		present = StrUtil.capitalizeFirst(present);
		
		JSONObject obj = getObj(e, present.toLowerCase());
		TextChannel target = e.getChannel();
		if (!e.getMessage().getMentionedChannels().isEmpty()) {
			target = e.getMessage().getMentionedChannels().get(0);
		}
		boolean enabled = obj.has(target.getId());
		String out = present + " messages are currently disabled here.";
		if (enabled) {
			out = present + " messages are currently enabled here.\nMessage:```" + obj.get(target.getId()) + "```";
		}
		e.sendMessage(out);
	}
	
	private void joinLeaveMessage(Command e, String present) {
		
		present = present.toLowerCase();
		
		String msg = e.getRestOfMessage(3);
		JSONObject obj = getObj(e, present);
		obj.put(e.getChannel().getId(), msg);
		e.sendMessage("Successfully changed the message to " + msg);
		writeObj(e, present, obj);
	}
	
	private void joinLeaveManual(Command e, String present, String past) {
		
		present = present.toLowerCase();
		past = past.toLowerCase();
		
		ManualBuilder man = new ManualBuilder("Logging " + StrUtil.capitalizeFirst(present));
		man.setAuthor("Nathan")
			.setVersion("1.0")
			.setDescription("Edit aspects of " + present + " logging")
			.addArg("status", "Get the " + present + " message settings for this channel or a mentioned channel.")
			.addArg("<enable|disable>", "Enable or disable " + present + " notifications in this channel")
			.addArg("message %user% has " + past + " %guild%.", "Set the message to be displayed when a user has " + past + "." + "\n\tMessage Syntax:"
				+ "\n\t%user% - User's name as a mention" + "\n\t%id% - User's ID" + "\n\t%guild% - Guild's name" + "\n\t%username% - User's username"
				+ "\n\t%discriminator% - User's discriminator (User#1234, #1234 is the discriminator)");
		e.sendManual(man);
	}
	
	// END JOIN/LEAVE
	
	// START COMMON
	
	private JSONObject getObj(Command e, String subpart) {
		
		key.touchJSON(subpart + "-" + e.getGuild().getId(), new JSONObject());
		JSONObject obj = new JSONObject();
		obj = key.readJSON(subpart + "-" + e.getGuild().getId());
		return obj;
	}
	
	private void writeObj(Command e, String subpart, JSONObject obj) {
		
		key.writeJSON(subpart + "-" + e.getGuild().getId(), obj);
	}
}
