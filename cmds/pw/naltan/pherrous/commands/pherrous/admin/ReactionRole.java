package pw.naltan.pherrous.commands.pherrous.admin;

import java.io.File;

import org.json.JSONObject;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.MessageReaction.ReactionEmote;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class ReactionRole extends ListenerAdapter implements GuildCommand, Manual, Indexable, Assets, Passthrough {
	
	@Override
	public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent e) {
		
		if (e.getUser().isFake() || e.getUser().isBot()) {
			return;
		}
		
		//Output.outAsText(e.getChannel(), e.getReactionEmote().getName());
		
		if (hookExists(e.getGuild().getId(), e.getMessageId())) {
			
			String snowflake = e.getReaction().getReactionEmote().getId();
			if (snowflake == null) {
				snowflake = e.getReaction().getReactionEmote().getName();
			}
			//			Output.outAsText(e.getChannel(), "Hit, Hook exists. Snowflake: " + snowflake);
			
			JSONObject obj = getObj(e.getGuild(), e.getMessageId());
			for (String current : obj.keySet()) {
				if (current.equals(snowflake)) {
					Role assign = e.getGuild().getRoleById(Long.parseLong((String) obj.get(current)));
					Member target = e.getGuild().getMember(e.getUser());
					if (target.getRoles().contains(assign)) {
						target.getUser().openPrivateChannel().queue(channel -> {
							channel.sendMessage("You already have that role!").queue();
							return;
						});
					}
					e.getGuild().addRoleToMember(target, assign).queue();
					target.getUser().openPrivateChannel().queue(channel -> {
						channel.sendMessage("Role added. React again to have the role removed.").queue();
					});
				}
			}
		}
	}
	
	@Override
	public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent e) {
		
		if (e.getUser().isFake() || e.getUser().isBot()) {
			return;
		}
		
		if (hookExists(e.getGuild().getId(), e.getMessageId())) {
			String snowflake = e.getReaction().getReactionEmote().getId();
			if (snowflake == null) {
				snowflake = e.getReaction().getReactionEmote().getName();
			}
			//Output.outAsText(e.getChannel(), "Hit, Hook exists. Snowflake: " + snowflake);
			
			JSONObject obj = getObj(e.getGuild(), e.getMessageId());
			for (String current : obj.keySet()) {
				if (current.equals(snowflake)) {
					Role assign = e.getGuild().getRoleById(Long.parseLong(obj.getString(current)));
					Member target = e.getGuild().getMember(e.getUser());
					if (!target.getRoles().contains(assign)) {
						target.getUser().openPrivateChannel().queue(channel -> {
							channel.sendMessage("You don't have that role!").queue();
							return;
							
						});
						return;
					}
					e.getGuild().removeRoleFromMember(target, assign).queue();
					target.getUser().openPrivateChannel().queue(channel -> {
						channel.sendMessage("Role removed. React again to have the role added.").queue();;
					});
				}
			}
		}
	}
	
	private boolean hookExists(String guildid, String msgid) {
		
		String filename = msgid + "-" + guildid + ".json";
		File f = new File(key.getDir() + filename);
		if (f.exists() && !f.isDirectory()) {
			return true;
		}
		return false;
	}
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Add or remove react assign hooks. Admin+ Only")
			.setAuthor("Nathan")
			.setVersion("1.0")
			.addArg("add #channel <message id> <emoji> @Role", "Add an assign hook to <message id> in <channel> for role @Role with reaction <reaction>")
			.addArg("remove #channel <message id> <emoji|@Role>", "Remove an assign trigger by its emote or its role.")
			.addArg("status #channel <message id>", "Get the status of react emotes on a message");
	}
	
	@Override
	public String name() {
		
		return "reactionrole";
	}
	
	@Override
	public void cmd(Command e) {
		
		if (!e.getMember().hasPermission(Permission.ADMINISTRATOR)) {
			e.sendMessage("Only Administrators can use this command.");
			return;
		}
		switch (e.getIndex(1)) {
			case "add" :
				add(e);
				return;
			
			case "remove" :
				remove(e);
				return;
			
			case "status" :
				status(e);
				return;
			
			default :
				e.sendMessage("Invalid syntax. Check the manual for help");
		}
	}
	
	private void add(Command e) {
		
		TextChannel chan = null;
		String emote = "";
		Message msg = null;
		Emote emoji = null;
		Role role = null;
		boolean unicodeEmote = false;
		try {
			chan = e.getMessage().getMentionedChannels().get(0);
			role = e.getMessage().getMentionedRoles().get(0);
			String id = e.getMessage().getContentRaw().split(" ")[3];
			msg = chan.retrieveMessageById(id).complete();
			if (!e.getMessage().getEmotes().isEmpty()) {
				if (e.getMessage().getEmotes().get(0).isFake()) {
					throw new NumberFormatException();
				}
				emote = e.getMessage().getEmotes().get(0).getAsMention();
				emoji = e.getMessage().getEmotes().get(0);
			}
			if (e.getMessage().getEmotes().isEmpty()) {
				emote = e.getMessage().getContentRaw().split(" ")[4];
				unicodeEmote = true;
			}
		} catch (NumberFormatException ex) {
			e.sendMessage("You can only use Unicode emotes or emotes from this server.");
			return;
		} catch (Exception ex) {
			ex.printStackTrace();
			e.sendMessage("Invaid syntax. Check the manual for help");
			return;
		}
		
		String write = "";
		JSONObject obj = getObj(e.getGuild(), msg.getId());
		
		if (!unicodeEmote) {
			write = emoji.getId();
		} else {
			write = emote;
		}
		
		if (obj.keySet().contains(write)) {
			e.sendMessage("That emote is already being used.");
			return;
		}
		
		obj.put(write, role.getId());
		writeObj(e, msg.getId(), obj);
		
		try {
			if (!unicodeEmote) {
				msg.addReaction(emoji).queue();
				write = emoji.getId();
			} else {
				msg.addReaction(emote).queue();
				write = emote;
			}
		} catch (ErrorResponseException ex) {
			e.sendMessage("Invalid message id. Did you copy it correctly?");
			return;
		}
		e.addReaction("👍");
	}
	//0				1		2		3	4
	//reactassign remove #channel <id> @role
	
	private void remove(Command e) {
		
		TextChannel channel = null;
		Message msg = null;
		Role role = null;
		try {
			channel = e.getMessage().getMentionedChannels().get(0);
			String id = e.getMessage().getContentRaw().split(" ")[3];
			msg = channel.retrieveMessageById(id).complete();
			role = e.getMessage().getMentionedRoles().get(0);
		} catch (Exception ex) {
			e.sendMessage("Invalid syntax. Check the manual for help.");
			return;
		}
		String thefuckingshittobedeleted = "";
		JSONObject obj = getObj(e.getGuild(), msg.getId());
		for (Object currentObj : obj.keySet()) {
			String current = (String) currentObj;
			if (obj.get(current).equals(role.getId())) {
				String deleted = (String) obj.get(current);
				thefuckingshittobedeleted = current;
				if (deleted == null) {
					e.sendMessage("Deletion failed. Was that role bound?");
					return;
				}
				
				boolean isEmote = true;
				try {
					Long.parseLong(current);
				} catch (Exception ex) {
					isEmote = false;
				}
				
				for (MessageReaction currentr : msg.getReactions()) {
					
					try {
						if (currentr.getReactionEmote().getId().equals(current) && isEmote) {
							currentr.removeReaction(e.getGuild().getSelfMember().getUser()).queue();
						} else if (currentr.getReactionEmote().getName().equals(current)) {
							currentr.removeReaction(e.getGuild().getSelfMember().getUser()).queue();
						}
					} catch (NullPointerException ex) {
						if (currentr.getReactionEmote().getName().equals(current)) {
							currentr.removeReaction(e.getGuild().getSelfMember().getUser()).queue();
						}
					}
				}
				
				if (obj.isEmpty()) {
					File f = new File(key.getDir() + msg.getId() + "-" + e.getGuild().getId() + ".json");
					f.delete();
					e.addReaction("👍");
					return;
				}
			}
		}
		obj.remove(thefuckingshittobedeleted);
		writeObj(e, msg.getId(), obj);
		
		e.addReaction("👍");
	}
	
	/*
	private void remove(Command e) {
		TextChannel channel = null;
		Message msg = null;
		String deletThis = "";		
		try {
			channel = e.getMessage().getMentionedChannels().get(0);
			String id = e.getMessage().getContentRaw().split(" ")[3];
			msg = channel.getMessageById(id).complete();
			try {
				deletThis = e.getMessage().getMentionedRoles().get(0).getId();
			} catch (Exception ex) {
				try {
					deletThis = e.getMessage().getEmotes().get(0).getId();
				} catch (Exception exc) {
					deletThis = e.getMessage().getContentRaw().split(" ")[4];
				}
			}
		} catch (Exception ex) {
			Output.outAsText(e.getChannel(), "Invalid syntax. Check the manual for help.");
			return;
		}
		
		JSONObject obj = getObj(e.getGuild(), msg.getId());
		boolean success = obj.remove(deletThis, obj.get(deletThis));
		String out = "Removal failed. Was the reaction on the message?";
		String oof = "";
		
		if(success) {
			out = "Removal succeeded.";
			List<MessageReaction> reacts = msg.getReactions();
			
			//aaa this needs to remove the reaction by the bot.
			
			for(MessageReaction current : reacts) {
				oof = current.getReactionEmote().getName();
				if(oof.startsWith(":")) {	//unicode emote
					oof = e.getMessage().getContentRaw().split(" ")[4];
				} else {
					oof = e.getMessage().getEmotes().get(0).getId();
				}
				if(oof.equals(deletThis)) {
					current.removeReaction().queue();
				}
			}
		}
		Output.outAsText(e.getChannel(), out);
		if(!obj.isEmpty()) {
			writeObj(e, msg.getId(), obj);
			return;
		} else {
			File f = new File(key.getDir() + msg.getId() + "-" + e.getGuild().getId() + ".json");
			f.delete();
			return;
		}
	}
	*/
	
	//0				1		2	3
	//~reactassign status #fuck <me>
	private void status(Command e) {
		
		TextChannel chan = null;
		String msg = "";
		Message txtMsg = null;
		try {
			chan = e.getMessage().getMentionedChannels().get(0);
			msg = e.getMessage().getContentRaw().split(" ")[3];
			txtMsg = chan.retrieveMessageById(msg).complete();
			if (txtMsg == null) {
				e.sendMessage("That message doesn't exist.");
				return;
			}
		} catch (Exception ex) {
			e.sendMessage("Invalid syntax. Check the manual for help.");
			return;
		}
		File f = new File(key.getDir() + msg + "-" + e.getGuild().getId() + ".json");
		if (!f.exists()) {
			e.sendMessage("There isn't a reaction trigger for that.");
			return;
		}
		JSONObject obj = getObj(e.getGuild(), chan.retrieveMessageById(msg).complete().getId());
		String out = "List of hooks:```";
		
		for (String current : obj.keySet()) {
			String vanity = "";
			for (MessageReaction currentR : txtMsg.getReactions()) {
				ReactionEmote temp = currentR.getReactionEmote();
				try {
					if (temp.getId().equals(current)) {
						vanity = temp.getName();
					}
				} catch (NullPointerException ex) {
					if (temp.getName().equals(current)) {
						vanity = temp.getName();
					}
				}
			}
			out += "\n" + vanity + " - " + e.getGuild().getRoleById(obj.getString(current)).getName();
		}
		out += "```";
		if (out.equals("List of hooks:``````")) {
			out = "There are no role bindings on this message";
		}
		
		EmbedBuilder eb = new EmbedBuilder();
		
		eb.setTitle("Current hooks on message " + txtMsg.getId()).setDescription(out);;
		e.sendMessage(eb);
	}
	
	private JSONObject getObj(Guild e, String subpart) {
		
		key.touchJSON(subpart + "-" + e.getId(), new JSONObject());
		JSONObject obj = new JSONObject();
		obj = key.readJSON(subpart + "-" + e.getId());
		return obj;
	}
	
	private void writeObj(Command e, String subpart, JSONObject obj) {
		
		key.writeJSON(subpart + "-" + e.getGuild().getId(), obj);
	}
	
	@Override
	public String assetDir() {
		
		return "reactionrole";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		ReactionRole.key = key;
	}
	
}
