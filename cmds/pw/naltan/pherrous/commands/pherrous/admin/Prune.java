package pw.naltan.pherrous.commands.pherrous.admin;

import java.util.ArrayList;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class Prune implements GuildCommand, Manual, Indexable {
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Prune messages. Message Channel perms required on executor.")
			.addArg("<number>", "Prunes a set amount of messages. Min 1, max 100.")
			.setAuthor("Nathan")
			.setVersion("1.0");
	}
	
	@Override
	public String name() {
		
		return "prune";
	}
	
	@Override
	public void cmd(Command e) {
		
		if (!e.getMember().hasPermission(Permission.MESSAGE_MANAGE)) {
			e.sendMessage("You need to have the \"Manage Messages\" permission in order to do that.");
			return;
		}
		
		try {
			
			int messages = 0;
			
			messages = Integer.parseInt(e.getIndex(1));
			
			if (messages <= 0) {
				messages = 1;
			} else if (messages > 100) {
				messages = 100;
			}
			
			MessageHistory hist = e.getChannel().getHistoryBefore(e.getMessage(), messages).complete();
			
			ArrayList<String> ids = new ArrayList<String>();
			for (Message current : hist.getRetrievedHistory()) {
				ids.add(current.getId());
			}
			
			e.getMessage().delete().queue();
			
			e.getChannel().deleteMessagesByIds(ids).queue();
			
			e.sendTempMessage("Deleted " + messages + " messages.", 3000);
			
		} catch (NumberFormatException ex) {
			e.sendTempMessage("Enter a number of messages to delete", 2000);
		} catch (IllegalArgumentException ex) {
			e.sendTempMessage("ERROR: One of the messages specified is >2 weeks old. I can't manage messages older than that.", 4000);
		}
		
	}
}
