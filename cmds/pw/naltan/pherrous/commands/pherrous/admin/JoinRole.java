package pw.naltan.pherrous.commands.pherrous.admin;

//TODO: finish this

import org.json.JSONArray;
import org.json.JSONObject;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.exceptions.PermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import pw.naltan.pherrous.interfaces.Assets;
import pw.naltan.pherrous.interfaces.GuildCommand;
import pw.naltan.pherrous.interfaces.Indexable;
import pw.naltan.pherrous.interfaces.Manual;
import pw.naltan.pherrous.interfaces.Passthrough;
import pw.naltan.pherrous.objects.AssetKey;
import pw.naltan.pherrous.objects.Command;
import pw.naltan.pherrous.objects.ManualBuilder;

public class JoinRole extends ListenerAdapter implements GuildCommand, Manual, Indexable, Assets, Passthrough {
	
	@Override
	public String category() {
		
		return "admin";
	}
	
	@Override
	public ManualBuilder man(ManualBuilder man) {
		
		return man.setDescription("Make Pherrous automatically assign roles to a user when they join. Admin Only.");
	}
	
	@Override
	public String name() {
		
		return "joinrole";
	}
	
	@Override
	public void cmd(Command e) {
		
		try {
			switch (e.getIndex(1)) {
				case "add" :
					add(e);
					
				case "remove" :
					perm(e.getMember());
					
				case "list" :
					perm(e.getMember());
			}
		} catch (PermissionException ex) {
			e.sendMessage("You do not have permission to use that command");
		}
	}
	
	private void add(Command e) {
		
		//Read the json
		JSONObject def = new JSONObject();
		def.put("roles", new JSONArray());
		key.touchJSON(e.getGuild().getId(), def);
		
		//Check to see if the role to add already exists
		
		//Add the role's ID to the json file
		
		//Report success and return
		
	}
	
	private void perm(Member e) {
		
		if (!e.hasPermission(Permission.ADMINISTRATOR)) {
			throw new PermissionException("");
		}
	}
	
	@Override
	public String assetDir() {
		
		return "joinrole";
	}
	
	private static AssetKey key;
	
	@Override
	public void key(AssetKey key) {
		
		JoinRole.key = key;
	}
	
}
