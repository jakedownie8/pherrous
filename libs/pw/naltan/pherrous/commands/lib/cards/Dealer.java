package pw.naltan.pherrous.commands.lib.cards;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Dealer {
	
	private static final Card[] ALL = {
		Card.ACE_SPADE,
		Card.TWO_SPADE,
		Card.THREE_SPADE,
		Card.FOUR_SPADE,
		Card.FIVE_SPADE,
		Card.SIX_SPADE,
		Card.SEVEN_SPADE,
		Card.EIGHT_SPADE,
		Card.NINE_SPADE,
		Card.TEN_SPADE,
		Card.JACK_SPADE,
		Card.QUEEN_SPADE,
		Card.KING_SPADE,
		
		Card.ACE_HEART,
		Card.TWO_HEART,
		Card.THREE_HEART,
		Card.FOUR_HEART,
		Card.FIVE_HEART,
		Card.SIX_HEART,
		Card.SEVEN_HEART,
		Card.EIGHT_HEART,
		Card.NINE_HEART,
		Card.TEN_HEART,
		Card.JACK_HEART,
		Card.QUEEN_HEART,
		Card.KING_HEART,
		
		Card.ACE_CLUB,
		Card.TWO_CLUB,
		Card.THREE_CLUB,
		Card.FOUR_CLUB,
		Card.FIVE_CLUB,
		Card.SIX_CLUB,
		Card.SEVEN_CLUB,
		Card.EIGHT_CLUB,
		Card.NINE_CLUB,
		Card.TEN_CLUB,
		Card.JACK_CLUB,
		Card.QUEEN_CLUB,
		Card.KING_CLUB,
		
		Card.ACE_DIAMOND,
		Card.TWO_DIAMOND,
		Card.THREE_DIAMOND,
		Card.FOUR_DIAMOND,
		Card.FIVE_DIAMOND,
		Card.SIX_DIAMOND,
		Card.SEVEN_DIAMOND,
		Card.EIGHT_DIAMOND,
		Card.NINE_DIAMOND,
		Card.TEN_DIAMOND,
		Card.JACK_DIAMOND,
		Card.QUEEN_DIAMOND,
		Card.KING_DIAMOND,
	};
	
	private List<Card> cards = new ArrayList<Card>();
	private List<Card> drawn = new ArrayList<Card>();
	
	/**
	 * Initialize a new dealer with <b>decks<b> decks.
	 * @param decks - the number of decks to use
	 * @param useJokers - whether jokers should be added or not.
	 */
	public Dealer(int decks, boolean useJokers) {
		
		for (int i = 0; i < decks; i++) {
			for (Card current : ALL) {
				cards.add(current);
			}
			
			if (useJokers) {
				cards.add(Card.JOKER_1);
				cards.add(Card.JOKER_2);
			}
		}
	}
	
	/**
	 * Initialize a new dealer with <b>decks<b> decks that does NOT use jokers.
	 * @param decks - the amount of decks to use
	 */
	public Dealer(int decks) {
		
		this(decks, false);
	}
	
	/**
	 * Initialize a new Dealer with 1 deck that does NOT use jokers
	 */
	public Dealer() {
		
		this(1);
	}
	
	public Card drawCard() {
		
		try {
			Card cd = cards.get(new Random().nextInt(cards.size()));
			cards.remove(cd);
			drawn.add(cd);
			return cd;
		} catch (Exception ex) {
			return null;
		}
		
	}
	
	public List<Card> getDrawn() {
		
		return drawn;
	}
	
	public List<Card> getDeck() {
		
		return cards;
	}
	
}
