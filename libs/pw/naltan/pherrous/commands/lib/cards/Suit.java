package pw.naltan.pherrous.commands.lib.cards;

public enum Suit {
	SPADES("Spades"),
	HEARTS("Hearts"),
	DIAMONDS("Diamonds"),
	CLUBS("Clubs"),
	JOKER("Joker"),
	
	;
	
	private String name;
	
	Suit(String name) {
		
		this.name = name;
	}
	
	public String getName() {
		
		return name;
	}
	
}
