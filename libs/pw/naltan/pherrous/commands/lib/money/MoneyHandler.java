package pw.naltan.pherrous.commands.lib.money;

import java.math.BigInteger;

import org.json.JSONObject;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import pw.naltan.pherrous.objects.AssetKey;

public class MoneyHandler {
	
	/*
	 * JSON values
	 * "default" : default balance
	 * "<userid>" : user's money
	 * "icon" : icon for the money
	 */
	
	private String discrim;
	
	JSONObject obj;
	
	private AssetKey key;
	
	private String icon;
	
	private boolean isClosed = false;
	
	private BigInteger starting;
	
	public MoneyHandler(String discrim) throws IllegalArgumentException {
		
		if (discrim == null) {
			throw new IllegalArgumentException("The discriminator must not be null!");
		}
		
		AssetKey key = new AssetKey("money");
		obj = key.readJSON(discrim);
		
		this.discrim = discrim;
		this.key = key;
		
		setData();
	}
	
	private void setData() {
		
		boolean rewrite = false;
		try {
			starting = new BigInteger((String) obj.get("default"));
		} catch (Exception ex) {
			rewrite = true;
			starting = BigInteger.valueOf(100);
			obj.put("default", starting.toString());
		}
		
		try {
			icon = (String) obj.get("icon");
			if (icon == null) {
				throw new Exception();
			}
		} catch (Exception ex) {
			icon = ":dollar:";
			obj.put("icon", icon);
			rewrite = true;
		}
		rewrite(rewrite);
	}
	
	public void init() {
		
		icon = ":dollar:";
		starting = BigInteger.valueOf(100);
	}
	
	public String getIcon() {
		
		return icon;
	}
	
	public BigInteger getDefault() {
		
		return starting;
	}
	
	public BigInteger getMoney(String discrim) {
		
		isClosed = false;
		//gets the string from the json
		String in = (String) obj.get(discrim);
		
		if (in == null) {
			//if its null, set the value in the json, rewrite the json, and in = the starting value
			obj.put(discrim, starting.toString());
			rewrite(true);
			in = starting.toString();
		}
		return new BigInteger(in);
	}
	
	public boolean addMoney(Member m, String discrim, BigInteger add) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			BigInteger start = getMoney(discrim);
			start = start.add(add);
			
			if (isNegative(add)) {
				return false;
			}
			
			obj.put(discrim, start.toString());
			rewrite(true);
			return true;
		}
	}
	
	public boolean addMoney(String discrim, BigInteger add) {
		
		exc();
		
		BigInteger start = getMoney(discrim);
		start = start.add(add);
		
		if (isNegative(add)) {
			return false;
		}
		
		obj.put(discrim, start.toString());
		rewrite(true);
		return true;
	}
	
	public boolean deductMoney(Member m, String discrim, BigInteger rm) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			BigInteger start = getMoney(discrim);
			start = start.subtract(rm);
			
			if (isNegative(start)) {
				return false;
			}
			
			obj.put(discrim, start.toString());
			rewrite(true);
			return true;
		}
	}
	
	public boolean deductMoney(String discrim, BigInteger rm) {
		
		exc();
		
		BigInteger start = getMoney(discrim);
		start = start.subtract(rm);
		
		if (isNegative(start)) {
			return false;
		}
		
		obj.put(discrim, start.toString());
		rewrite(true);
		return true;
	}
	
	public boolean setMoney(Member m, String discrim, BigInteger rm) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			obj.put(discrim, rm.toString());
			return true;
		}
	}
	
	public boolean resetMoney(Member m, String discrim) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			return setMoney(m, discrim, starting);
		}
	}
	
	public boolean setIcon(Member m, String icon) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			this.icon = icon;
			obj.put("icon", icon);
			rewrite(true);
			return true;
		}
	}
	
	public boolean setDefault(Member m, BigInteger val) {
		
		exc();
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			starting = val;
			obj.put("default", val.toString());
			rewrite(true);
			return true;
		}
	}
	
	public void close() {
		
		isClosed = true;
		discrim = null;
		obj = null;
		key = null;
		icon = null;
		starting = null;
	}
	
	public boolean resetAll(Member m) {
		
		if (!m.hasPermission(Permission.ADMINISTRATOR)) {
			return false;
		} else {
			obj = new JSONObject();
			obj.put("icon", icon);
			obj.put("default", starting);
			rewrite(true);
			return true;
		}
	}
	
	private void exc() {
		
		if (isClosed) {
			throw new IllegalStateException("This object has been closed");
		}
	}
	
	private void rewrite(boolean rewrite) {
		
		if (rewrite) {
			key.writeJSON(this.discrim, obj);
		}
	}
	
	public static boolean isNegative(BigInteger in) {
		
		return in.compareTo(new BigInteger("0")) < 0;
	}
}
