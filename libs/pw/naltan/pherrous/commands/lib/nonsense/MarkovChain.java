package pw.naltan.pherrous.commands.lib.nonsense;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

import org.json.JSONObject;

import pw.naltan.pherrous.core.output.Console;
import pw.naltan.pherrous.objects.AssetKey;

public class MarkovChain {
	
	private Random r;
	private int keySize, outputSize;
	private ArrayList<File> files;
	private HashMap<String, List<String>> map;
	
	public MarkovChain(ArrayList<File> files, int keysize, int outputSize) {
		
		r = new Random(System.nanoTime() * 502475934 - 69 /*nice*/);
		
		this.files = files;
		this.keySize = keysize;
		this.outputSize = outputSize;
	}
	
	public void map() throws IOException {
		
		HashMap<String, List<String>> dict = new HashMap<>();
		
		if (keySize < 1)
			throw new IllegalArgumentException("Key size can't be less than 1");
		for (File current : files) {
			String filePath = current.getAbsolutePath();
			
			Path path = Paths.get(filePath);
			byte[] bytes = Files.readAllBytes(path);
			String[] words = new String(bytes).trim().split(" ");
			
			// Make sure everything's in bounds, else toss it out.
			if (outputSize >= keySize && outputSize < words.length) {
				
				for (int i = 0; i < (words.length - keySize); ++i) {
					StringBuilder key = new StringBuilder(words[i]);
					for (int j = i + 1; j < i + keySize; ++j) {
						key.append(' ').append(words[j]);
					}
					String value = (i + keySize < words.length) ? words[i + keySize] : "";
					if (!dict.containsKey(key.toString())) {
						ArrayList<String> list = new ArrayList<>();
						list.add(value);
						dict.put(key.toString(), list);
					} else {
						dict.get(key.toString()).add(value);
					}
				}
			}
		}
		//END MARKOV PARSING
		
		// Why is this try/catch here? because fuck java.
		try {
			map = dict;
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}
	
	public HashMap<String, List<String>> getMap() {
		
		return map;
	}
	
	public String nextString() {
		
		int n = 0;
		int rn = r.nextInt(map.size() - 1);
		String prefix = (String) map.keySet().toArray()[rn];
		List<String> output = new ArrayList<>(Arrays.asList(prefix.split(" ")));
		
		while (true) {
			
			List<String> suffix = map.get(prefix);
			if (suffix == null) {
				Console.debug("null suffix");
			}
			if (suffix.size() == 1) {
				if (Objects.equals(suffix.get(0), ""))
					return output.stream().reduce("", (a, b) -> a + " " + b);
				output.add(suffix.get(0));
			} else {
				rn = r.nextInt(suffix.size());
				output.add(suffix.get(rn));
			}
			if (output.size() >= outputSize)
				return output.stream().limit(outputSize).reduce("", (a, b) -> a + " " + b);
			n++;
			prefix = output.stream().skip(n).limit(keySize).reduce("", (a, b) -> a + " " + b).trim();
		}
	}
	
	public void saveChain(String name, AssetKey akey) throws IOException {
		
		JSONObject obj = new JSONObject();
		
		for (String key : map.keySet()) {
			obj.put(key, map.get(key));
		}
		
		//System.out.println(obj.toString(1));
		FileWriter fw = new FileWriter(new File(akey.getDir() + name + ".json"));
		fw.write(obj.toString(1));
		fw.close();
		
		akey.writeJSON(name, obj);
	}
	
	public Map<String, List<String>> loadChain(String name, AssetKey key) throws IOException {
		
		Map<String, List<String>> out = new HashMap<>();
		
		Scanner s = new Scanner(new File(key.getDir() + name + ".json"));
		String in = "";
		
		while (s.hasNext()) {
			in += s.nextLine();
		}
		s.close();
		
		JSONObject obj = new JSONObject(in);
		System.out.println(obj);
		for (String current : obj.keySet()) {
			
			Console.debug(current);
			
			List<Object> objects = obj.getJSONArray(current).toList();
			List<String> strings = new ArrayList<>();
			
			for (Object currentobj : objects) {
				strings.add((String) currentobj);
			}
			
			out.put(current, strings);
		}
		return out;
	}
	
	public void close() {
		
		map = null;
		r = null;
		keySize = 0;
		outputSize = 0;
		files = null;
	}
	
	public void closeFileList() {
		
		files = null;
	}
}
