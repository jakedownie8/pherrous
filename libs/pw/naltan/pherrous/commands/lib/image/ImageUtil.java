package pw.naltan.pherrous.commands.lib.image;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.ImageRoi;
import ij.gui.Overlay;
import ij.io.FileSaver;
import ij.process.ImageProcessor;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.entities.User;

public class ImageUtil {
	
	/**
	 * Overlay an image on top of another one. (DOES NOT PRESERVE ASPECT RATIO OF OVERLAY!)
	 * @param overlay - The image to overlay
	 * @param bg - The background image
	 * @param out - The output file
	 * @param x - The x coordinate to place the overlay image at, relative to the top left
	 * @param y - The y coordinate to place the overlay image ar, relative to the top left
	 * @param width - The width to make the overlay
	 * @param height - The height to make the overlay
	 */
	public static void overlay(String overlay, String bg, File out, int x, int y, int width, int height) {
		
		//open and process overlay
		ImagePlus olay = IJ.openImage(overlay);
		ImageProcessor ps = olay.getProcessor();
		
		ps = ps.resize(width, height);
		
		//overlay meta
		ImageRoi roi = new ImageRoi(x, y, ps);
		
		roi.setZeroTransparent(false);
		roi.setOpacity(1);
		
		ImagePlus back = IJ.openImage(bg);
		back.setOverlay(new Overlay(roi));
		back.flatten();
		
		//back.show();
		
		FileSaver fs = new FileSaver(back);
		fs.saveAsPng(out.getAbsolutePath());
	}
	
	/**
	 * Get the last effective image from the last 50 messages. Iterates through the following cases in order per each message.
	 * <br>
	 * <b>NOTE:</b> The file extension will automatically be added to the file. Don't give your file an extension (i.e. new File("foo");)
	 * <br><br>
	 * <b> - Case 1:</b> Image is posted directly (via "Upload image" button)
	 * <br>
	 * <b> - Case 2:</b> Image is posted via a link (scans for a url that starts with http or https, and strips away queries such as ?size=2048)
	 * <br>
	 * <b> - Case 3:</b> Image is actually a user's avatar (User is mentioned)
	 * <br>
	 * <b> - Case 4:</b> Image is an emote (discord custom emojis)
	 * <br>
	 * <b> - Case 5:</b> Image is an emoji (actual unicode emojis)
	 * @param e - The message to scan messages prior to.
	 * @param f - The file to download to.
	 * @return The file that contains the image, or null if it doesn't exist.
	 */
	public static File getLastEffectiveImage(Message e, String f) {
		
		//Retrieve all messages
		ArrayList<Message> messages = new ArrayList<>();
		messages.add(e);
		messages.addAll(e.getChannel().getHistoryBefore(e, 49).complete().getRetrievedHistory());
		
		//For each file, try to get the image, and if failiure, just return.
		for (Message current : messages) {
			File attempt = getImage(current, f);
			if (attempt != null) {
				return attempt;
			}
		}
		return null;
	}
	
	private static File getImage(Message e, String f) {
		
		//CASE 1: Image in the message attachments
		//TEST PASS
		for (Attachment current : e.getAttachments()) {
			if (current.isImage()) {
				return downloadFile(f, current.getUrl());
			}
		}
		
		//CASE 2: Image posted via link
		String content = e.getContentDisplay().toLowerCase();
		for (String current : content.split(" ")) {
			//Strip away queries (?size=2048);
			int quer = current.indexOf("?");
			if (quer != -1) {
				content = content.substring(0, quer);
			}
			
			if (startUrl(current) && endUrl(current)) {
				return downloadFile(f, content);
				
			}
		}
		
		//CASE 3: Mentioned user
		//TEST PASS
		for (User current : e.getMentionedUsers()) {
			String url = current.getEffectiveAvatarUrl() + "?size=2048";
			return downloadFile(f, url);
		}
		
		//CASE 4: Emote
		//TEST PASS
		for (Emote current : e.getEmotes()) {
			if (!current.isAnimated()) {
				String emote = current.getImageUrl();
				return downloadFile(f, emote);
			}
		}
		
		//CASE 5: Emoji
		
		//CASE 0: No images.
		return null;
	}
	
	private static boolean startUrl(String url) {
		
		String[] strs = {
			"http://",
			"https://"
		};
		for (String current : strs) {
			if (url.startsWith(current)) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean endUrl(String url) {
		
		String[] strs = {
			"jpg",
			"png",
		};
		for (String current : strs) {
			if (url.endsWith(current)) {
				return true;
			}
		}
		return false;
	}
	
	private static File downloadFile(String f, String url) {
		
		try {
			URL urlobj = new URL(url);
			URLConnection conn = urlobj.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
			BufferedInputStream inputStream = new BufferedInputStream(conn.getInputStream());
			
			String ext = url.substring(url.lastIndexOf("."));
			
			//query after url
			if (url.indexOf("?") != -1) {
				ext = ext.substring(0, ext.indexOf("?"));
			}
			
			File out = new File(f + ext);
			
			FileOutputStream fileOS = new FileOutputStream(out);
			byte data[] = new byte[1024];
			int byteContent;
			while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
				fileOS.write(data, 0, byteContent);
			}
			fileOS.close();
			
			return out;
			
		} catch (MalformedURLException ex) {
			return null;
			
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}
